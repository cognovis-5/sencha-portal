-- upgrade-4.0.0.2.2-4.0.0.2.3.sql
SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.2.2-4.0.0.2.3.sql','');

UPDATE im_view_columns SET ajax_configuration = 'filterType:list,defaultFilterValue:[im_name_from_id 76]' WHERE column_id = 103038;
UPDATE im_view_columns SET ajax_configuration = 'hidden:true' WHERE column_id = 103052;
UPDATE im_view_columns SET ajax_configuration = 'hidden:true' WHERE column_id = 103054;
UPDATE im_view_columns SET ajax_configuration = 'filterType:list,defaultFilterValue:loggedUser' WHERE column_id = 103056;

