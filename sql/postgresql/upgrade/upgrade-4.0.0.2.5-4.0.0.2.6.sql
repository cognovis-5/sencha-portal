-- upgrade-4.0.0.2.5-4.0.0.2.6.sql
SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.2.5-4.0.0.2.6.sql','');


-- Main menu
select im_menu__new (
        1056000,              -- p_menu_id
        'acs_object',  -- object_type
        now(),            -- creation_date
        null,              -- creation_user
        null,              -- creation_ip
        null,              -- context_id
        'webix-portal',     -- package_name
        'Webix main menu', -- label
        'webix_main_menu',               -- name
        '', -- url
        60,                  -- sort_order
        475,  -- parent_menu_id
        '' -- p_visible_tcl
    );

-- admins
select acs_permission__grant_permission(1056000, 459, 'read');
-- po's
select acs_permission__grant_permission(1056000, 467, 'read');
-- customers
select acs_permission__grant_permission(1056000, 461, 'read');


-- Account info
select im_menu__new (
        1056002,              -- p_menu_id
        'acs_object',  -- object_type
        now(),            -- creation_date
        null,              -- creation_user
        null,              -- creation_ip
        null,              -- context_id
        'webix-portal',     -- package_name
        'Account Info', -- label
        'webix_account_info',               -- name
        'account-info.account-info', -- url
        10,                  -- sort_order
        1056000,  -- parent_menu_id
        '' -- p_visible_tcl
    );

-- AccountInfo permissions
-- admins
select acs_permission__grant_permission(1056002, 459, 'read');
-- po's
select acs_permission__grant_permission(1056002, 467, 'read');
-- customers
select acs_permission__grant_permission(1056002, 461, 'read');

-- Menu Icon
update im_menus set menu_gif_small = 'fas fa-info-circle' where menu_id = 1056002;



-- New offer
select im_menu__new (
        1056004,              -- p_menu_id
        'acs_object',  -- object_type
        now(),            -- creation_date
        null,              -- creation_user
        null,              -- creation_ip
        null,              -- context_id
        'webix-portal',     -- package_name
        'New Offer', -- label
        'webix_new_offer',               -- name
        'new_offer.new_offer', -- url
        15,                  -- sort_order
        1056000,  -- parent_menu_id
        '' -- p_visible_tcl
    );
-- NewOffer permissions
-- admins
select acs_permission__grant_permission(1056004, 459, 'read');
-- po's
select acs_permission__grant_permission(1056004, 467, 'read');
-- customers
select acs_permission__grant_permission(1056004, 461, 'read');

-- Menu Icon
update im_menus set menu_gif_small = 'fas fa-plus-square' where menu_id = 1056004;


-- Projects
select im_menu__new (
        1056006,              -- p_menu_id
        'acs_object',  -- object_type
        now(),            -- creation_date
        null,              -- creation_user
        null,              -- creation_ip
        null,              -- context_id
        'webix-portal',     -- package_name
        'Projects', -- label
        'webix_projects',               -- name
        'projects.projects', -- url
        20,                  -- sort_order
        1056000,  -- parent_menu_id
        '' -- p_visible_tcl
    );

-- Projects permissions
-- admins
select acs_permission__grant_permission(1056006, 459, 'read');
-- po's
select acs_permission__grant_permission(1056006, 467, 'read');
-- customers
select acs_permission__grant_permission(1056006, 461, 'read');

-- Menu Icon
update im_menus set menu_gif_small = 'fas fa-copy' where menu_id = 1056006;


-- Invoices
select im_menu__new (
        1056008,              -- p_menu_id
        'acs_object',  -- object_type
        now(),            -- creation_date
        null,              -- creation_user
        null,              -- creation_ip
        null,              -- context_id
        'webix-portal',     -- package_name
        'Invoices', -- label
        'webix_invoices',               -- name
        'invoices.invoices', -- url
        25,                  -- sort_order
        1056000,  -- parent_menu_id
        '' -- p_visible_tcl
    );

-- Invoices permissions
-- admins
select acs_permission__grant_permission(1056008, 459, 'read');
-- po's
select acs_permission__grant_permission(1056008, 467, 'read');
-- customers
select acs_permission__grant_permission(1056008, 461, 'read');

-- Menu Icon
update im_menus set menu_gif_small = 'fas fa-dollar-sign' where menu_id = 1056008;
