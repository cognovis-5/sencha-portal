-- upgrade-4.0.0.2.4-4.0.0.2.5.sql
SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.2.4-4.0.0.2.5.sql','');


UPDATE im_view_columns SET ajax_configuration = 'filterType:category,categoryTypeStore:projectStatuses,renderedValue:project_status_name,defaultFilterValue:76' WHERE column_id = 103038;
UPDATE im_view_columns SET extra_select = '' WHERE column_id = 103038;
UPDATE im_view_columns SET variable_name = 'project_status_id' WHERE column_id = 103038;
UPDATE im_view_columns SET datatype = 'integer' WHERE column_id = 103038;