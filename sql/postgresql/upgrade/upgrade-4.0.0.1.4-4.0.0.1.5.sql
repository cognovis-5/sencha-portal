-- upgrade-4.0.0.1.4-4.0.0.1.5.sql

SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.1.4-4.0.0.1.5.sql','');

create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
    select count(*) into v_count from im_categories
        where category_id = 1422;
    if v_count > 0 then return 0; end if;
    INSERT INTO im_categories (category_id,category,category_type,aux_string1, aux_string2, enabled_p, parent_only_p)
VALUES (1422,''Reports'',''Intranet DynView Type'',''reports'','''',''t'',''f'');
    return 0;
end;' language 'plpgsql';

select inline_0 ();
drop function inline_0 ();

-- Add project revenue view
--
delete from im_view_columns where view_id = 1030;
delete from im_views where view_id = 1030;
--

insert into im_views (view_id, view_name, view_label, view_type_id, view_sql)
values (1030, 'project_revenue_report', 'Project Revenue Reports', 1422, ' select p.* from im_projects p');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103001,1030,NULL,'#intranet-core.Project_nr#',
'"<A HREF=/intranet/projects/view?project_id=$project_id>$project_nr</A>"',
'','',5,'','project_nr','string', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103002,1030,NULL,'#intranet-core.Project_Name#',
'"<A HREF=/intranet/projects/view?project_id=$project_id>$project_name</A>"',
'','',10,'','project_name','string', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103003,1030,NULL,'#intranet-core.Client#',
'"<A HREF=/intranet/companies/view?company_id=$company_id>$company_name</A>"',
'','',15,'','company_name','string','display:company_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103004,1030,NULL,'#intranet-core.Project_Manager#',
'"<A HREF=/intranet/users/view?user_id=$project_lead_id>$lead_name</A>"',
'im_name_from_user_id(p.project_lead_id) as lead_name','',20,'','lead_name','string','display:person_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, sort_order, variable_name,datatype) values (103005,1030,NULL,'#intranet-core.Client# #intranet-core.Invoice#',
'"$invoice_amount"',
'(select round(sum(amount),2) from acs_rels r, im_costs where object_id_two = cost_id and cost_type_id = 3700 and object_id_one = p.project_id and effective_date::date >= :start_date and effective_date::date <= :end_date group by object_id_one) as invoice_amount',25,'invoice_amount','currency');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, sort_order,variable_name,datatype) values (103006,1030,NULL,'#intranet-core.Provider# #intranet-core.Invoice#',
'"$provider_amount"',
'(select round(sum(amount),2) from acs_rels r, im_costs where object_id_two = cost_id and cost_type_id = 3704 and object_id_one = p.project_id and effective_date::date >= :start_date and effective_date::date <= :end_date group by object_id_one) as provider_amount',30,'provider_amount','currency');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, sort_order, variable_name,datatype) values (103007,1030,NULL,'#intranet-timesheet2.Hours#',
'"$hours"','(select sum(hours) from im_hours where project_id = p.project_id and day::date >=:start_date and day::date <= :end_date) as hours',40,'hours','float');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, sort_order, variable_name,datatype) values (103008,1030,NULL,'#intranet-core.Provider#',
'"$provider_names"','(select array_to_string(array_agg(distinct company_name), '' - '') from acs_rels r, im_costs, im_companies co where object_id_two = cost_id and cost_type_id = 3704 and object_id_one = p.project_id and effective_date::date >= :start_date and effective_date::date <= :end_date and co.company_id = provider_id group by object_id_one) as provider_names',35,'provider_names','textarea');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, sort_order, variable_name,datatype) values (103009,1030,NULL,'#intranet-core.ProjectSourceName#',
'"$project_source_name"',' im_name_from_id(project_source_id) as project_source_name',45,'project_source_name','string');


