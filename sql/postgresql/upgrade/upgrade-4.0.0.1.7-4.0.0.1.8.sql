-- upgrade-4.0.0.1.7-4.0.0.1.8.sql
SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.1.7-4.0.0.1.8.sql','');


create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
    select count(*) into v_count from im_categories
        where category_id = 1430;
    if v_count > 0 then return 0; end if;
    INSERT INTO im_categories (category_id,category,category_type,aux_string1, aux_string2, enabled_p, parent_only_p)
VALUES (1430,''Freelancer projects'',''Intranet DynView Type'',''freelancer_projects'','''',''t'',''f'');
    return 0;
end;' language 'plpgsql';

select inline_0 ();
drop function inline_0 ();






-- Freelancer assigned projects dynview with columns
insert into im_views (view_id, view_name, view_label, view_type_id, view_sql)
values (1034, 'freelancer_assigned_projects', 'Freelancer assigned projects', 1430, 'select distinct p.project_nr, p.project_id, p.project_name, p.project_type_id,p.project_status_id, min(fa.end_date) as deadline, sum(assignment_units) as work_amount, min(uom_id) as uom_id, p.company_id from im_projects p, im_freelance_packages fp, im_freelance_assignments fa where fa.assignee_id =:assignee_id and fp.project_id = p.project_id and fp.freelance_package_id = fa.freelance_package_id and fa.assignment_status_id in (4222,4224,4225,4229) and p.project_status_id in (71,76) group by p.project_nr, p.company_id, p.project_id, p.project_name, p.project_type_id, p.project_status_id');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103020,1034,NULL,'#intranet-core.Project_nr#',
'"<A HREF=/intranet/projects/view?project_id=$project_id>$project_nr</A>"',
'','',5,'','project_nr','string', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103022,1034,NULL,'#intranet-core.Project_Name#',
'"<A HREF=/intranet/projects/view?project_id=$project_id>$project_name</A>"',
'','',10,'','project_name','string', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103024,1034,NULL,'#sencha-portal.work_amount#',
'$work_amount',
'','',15,'','work_amount','string', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103026,1034,NULL,'#sencha-freelance-translation.deadline#',
'$deadline',
'','',20,'','deadline','string', '');

-- Freelancer requested projects dynview with columns
insert into im_views (view_id, view_name, view_label, view_type_id, view_sql)
values (1036, 'freelancer_requested_projects', 'Freelancer requested projects', 1430, ' select distinct p.project_nr, p.project_id, p.project_name, p.project_type_id,p.project_status_id, min(fa.end_date) as deadline, sum(assignment_units) as work_amount, min(uom_id) as uom_id, p.company_id from im_projects p, im_freelance_packages fp, im_freelance_assignments fa where fa.assignee_id =:assignee_id and fp.project_id = p.project_id and fp.freelance_package_id = fa.freelance_package_id and fa.assignment_status_id in (4221) and p.project_status_id in (71,76) group by p.project_nr, p.company_id, p.project_id, p.project_name, p.project_type_id, p.project_status_id');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103028,1036,NULL,'#intranet-core.Project_nr#',
'"<A HREF=/intranet/projects/view?project_id=$project_id>$project_nr</A>"',
'','',5,'','project_nr','string', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103030,1036,NULL,'#intranet-core.Project_Name#',
'"<A HREF=/intranet/projects/view?project_id=$project_id>$project_name</A>"',
'','',10,'','project_name','string', 'display:project_link');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103032,1036,NULL,'#sencha-portal.work_amount#',
'$work_amount',
'','',15,'','work_amount','string', '');

insert into im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl,
extra_select, extra_where, sort_order, visible_for,variable_name,datatype,ajax_configuration) values (103034,1036,NULL,'#sencha-freelance-translation.deadline#',
'$deadline',
'','',20,'','deadline','string', '');

