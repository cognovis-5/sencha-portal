-- upgrade-4.0.0.1.5-4.0.0.1.6.sql
SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.1.5-4.0.0.1.6.sql','');


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin
     ALTER TABLE im_views ADD COLUMN sencha_configuration varchar;
return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();
