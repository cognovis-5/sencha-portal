SELECT acs_log__debug('/packages/sencha-portal/sql/postgresql/upgrade/upgrade-4.0.0.1.3-4.0.0.1.4.sql','');

-- Update the membership component to support sencha
update im_component_plugins set component_tcl = 'sencha_project_member_component -project_id $project_id -current_user_id $user_id' where plugin_id = 452;
