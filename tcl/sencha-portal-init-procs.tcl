
ad_library {
    INIT procedures for sencha-portal package

    @author malte.sussdorff@cognovis.de

}

ad_register_proc GET /sencha-portal/download/project/* sencha_portal_project_download
ad_register_proc GET /sencha-portal/download/user/* sencha_portal_user_download
