
ad_library {
    Helper procedures for sencha-portal package

    @author michaldrn@wp.pl

}


ad_proc -public im_project_id_from_assignment_id {
    -assignment_id
} { 
    Simple procedure which returns project_id of assignment
    It is useful as we do not have project_id column in `im_freelance_assignments` table
} {
    set project_id [db_string project_id_from_assignment_id_sql "select project_id from im_freelance_packages fp, im_freelance_assignments fa where fa.freelance_package_id = fp.freelance_package_id and fa.assignment_id =:assignment_id limit 1" -default 0]
    return $project_id
}


ad_proc -public assignment_previous_stage_id {
    -assignment_id
} {
    Procedure which returns id ('Intranet Trans Task Type') of previous stage for given assignment (@param assignment_id)
} {
    set assignment_type_id [db_string assignment_type_id "select assignment_type_id from im_freelance_assignments where assignment_id =:assignment_id" -default 0]
    set assignment_type_name [db_string assignment_type_name "select category from im_categories where category_id =:assignment_type_id" -default ""]
    if {$assignment_type_id ne 0} {
        set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]
        set project_type_id [db_string project_type_id_sql "select project_type_id from im_projects where project_id =:project_id" -default 0]
        set project_type_name [im_name_from_id $project_type_id]
        set project_stages_list [db_string project_stages_list_sql "select aux_string1 from im_categories where category_id =:project_type_id" -default ""]
        # Just in case we convert assignment_type_name to be lowercased
        set assignment_type_name [string tolower $assignment_type_name]
        set assignment_stage_in_project [lsearch $project_stages_list $assignment_type_name]
        if {$assignment_stage_in_project eq 0} {
            return -1;
        } else {
            set previous_assignment_stage [expr $assignment_stage_in_project -1]
            set previous_assignment_stage_name [lindex $project_stages_list $previous_assignment_stage]
            set previous_assignment_stage_id [db_string previous_assignment_stage_id_sql "select category_id from im_categories where category =:previous_assignment_stage_name" -default ""]
            return $previous_assignment_stage_id
        }
    }
}


ad_proc -public assignment_previous_stage_assignment_id {
    -assignment_id
} {
    Procedure which returns id of assignment of previous stage for given assignment (@param assignment_id)
} {

    set previous_stage_id [assignment_previous_stage_id -assignment_id $assignment_id]
    set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]

    # Get the package_ids for the previous_stage and compare the tasks
    set previous_freelance_package_ids [db_list previous_packages "select freelance_package_id from im_freelance_packages where project_id = :project_id and package_type_id = :previous_stage_id"]

    if {[llength $previous_freelance_package_ids] eq 0} {
    # No previous package found, so can't do this
    set previous_assignment_ids [list]
    } elseif {[llength $previous_freelance_package_ids] eq 1} {
    set previous_assignment_ids [db_string freelancers "select assignment_id from im_freelance_assignments  where freelance_package_id = :previous_freelance_package_ids and assignment_status_id not in (4223,4227,4228,4230)" -default ""]
    } else {
    # Need to look up the tasks for the freelance_package_id of the assignment and found out how many different freelancers worked on them.
    set assignment_task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks ptt, im_freelance_assignments fa 
                where fa.freelance_package_id = ptt.freelance_package_id
                and assignment_id = :assignment_id"]
        
    set previous_assignment_ids [db_list freelancers "select distinct assignment_id from im_freelance_assignments 
        where freelance_package_id in (select distinct freelance_package_id from im_freelance_packages_trans_tasks fptt
                       where fptt.trans_task_id in ([template::util::tcl_to_sql_list $assignment_task_ids])
                       and fptt.freelance_package_id in ([template::util::tcl_to_sql_list $previous_freelance_package_ids]))
        and assignment_status_id not in (4223,4227,4228,4230)"]
    }
    
    return $previous_assignment_ids
}



ad_proc -public assignment_stage_of_assignment {
    -assignment_id
} {
    Procedure which returns index ('Intranet Trans Task Type') of stage for given assignment (@param assignment_id)
} {

    set assignment_type_id [db_string assignment_type_id "select assignment_type_id from im_freelance_assignments where assignment_id =:assignment_id" -default 0]
    set assignment_type_name [db_string assignment_type_name "select category from im_categories where category_id =:assignment_type_id" -default ""]
    if {$assignment_type_id ne 0} {
        set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]
        set project_type_id [db_string project_type_id_sql "select project_type_id from im_projects where project_id =:project_id" -default 0]
        set project_type_name [im_category_from_id $project_type_id]
        set project_stages_list [db_string project_stages_list_sql "select aux_string1 from im_categories where category_id =:project_type_id" -default ""]
        # Just in case we convert assignment_type_name to be lowercased
        set assignment_type_name [string tolower $assignment_type_name]
        set assignment_stage_in_project [lsearch $project_stages_list $assignment_type_name]
        return $assignment_stage_in_project
    }
}


ad_proc -public assignment_freelancers_working_ids {
    -project_id
    {-package_type_id ""}
} {
    Procedure which returns ids of freelancer who has assignments on given project (project searched with @param project_id)
    @param package_type_id filters assignments to gicen package type
    Example: If we add package_type_id of 4220 (Trans), we will receive list of freelancers ids who are working on Trans assignments
} {
    # Check if package_type_id was provided
    if {$package_type_id eq ""} {
        set package_sql ""
    } else {
        set package_sql "and package_type_id =:package_type_id" 
    }

    set freelancers_working_on_project_ids [list]
    set project_packages [db_list project_packages_sql "select freelance_package_id from im_freelance_packages where project_id=:project_id $package_sql"]
    # We exclude assignments which have status: 4223(Denied), 4220(Assignment Deleted)
    set excluded_statuses_ids [list 4223 4230]
    if {[llength $project_packages] > 0} {
        set project_not_removed_assignments [db_list project_not_removed_assignments_sql "select assignee_id from im_freelance_assignments where freelance_package_id in ([template::util::tcl_to_sql_list $project_packages]) and assignment_status_id not in ([template::util::tcl_to_sql_list $excluded_statuses_ids])"]
        # Exclude duplicate
        set project_not_removed_assignments_unique [lsort -unique $project_not_removed_assignments]
        return $project_not_removed_assignments_unique
    }
}


ad_proc -public assignment_is_rating_fl_possible {
    -assignment_id
} { 
   Procedure checks if rating freelancer is possible for given assignment (found with @param assignment_id)
   First it checks assignment stage of project (e.g. Trans or Edit), as we can only rate assignments starting from second stage
   In addition procedure checks if only single freelancer worked on that package
   returns 1 in case rating is possible, returns 0 if rating is not possible
}  {

    set previous_assignment_ids [assignment_previous_stage_assignment_id -assignment_id $assignment_id]
    if { $previous_assignment_ids eq "" } {
    set rating_fl_possible_p 0
    } else {

    # Check if we have only one previous freelancer
    set previous_freelancer_ids [db_list freelancers "select distinct assignee_id from im_freelance_assignments where assignment_id in ([template::util::tcl_to_sql_list $previous_assignment_ids])"]
    if {[llength $previous_freelancer_ids] eq 1} {
        set rating_fl_possible_p 1
    } else {
        set rating_fl_possible_p 0
    }
    }
    return $rating_fl_possible_p
}



ad_proc -public im_user_main_company_id {
    -user_id
} {

    Return the main company id for given user_id.
    This should return single id or empty string
  
    @return company_id

} {
    # First we check if user is not primary_contact_id for 
    set company_id [db_string company_id_sql "select company_id from im_companies where primary_contact_id =:user_id limit 1" -default ""]
    # If company_id is still null, we keep on searching, this time with acs_rels
    if {$company_id eq ""} {
        set company_id [db_string company_id_sql "select object_id_one as company_id from acs_rels where object_id_two =:user_id and rel_type = 'im_company_employee_rel' limit 1" -default ""]
    }
    return $company_id

}


ad_proc -public im_company_key_account_id {
    -company_id
} {

    Return the key account id for given company_id
    This should return single id or empty string
  
    @return key_account_id

} {
    # First we check if user is not manager_id
    set key_account_id [db_string key_account_id_sql "select manager_id from im_companies where company_id =:company_id" -default ""]
    # If company id is null, we check acs_rels
    if {$company_id eq ""} {
        set key_account_id [db_string key_account_id_sql "select object_id_two as key_account_id from acs_rels where object_id_one =:company and rel_type = 'im_key_account_rel' limit 1" -default ""]
    }
    return $key_account_id

}


ad_proc -public im_category_ids_to_names {
    -category_ids
} {

    Converts list of category ids to list of category names

} {
    set category_names [list]
    foreach category_id $category_ids {
        lappend category_names [im_name_from_id $category_id]
    }

    return $category_names
}


ad_proc -public im_verify_user_token {
    -email
    -token
} {

    Procedure used to verify user token used for password reset

} {
    
    set password_token_matched_p [db_string user_id_from_email "select 1 from cc_users where email =:email and password =:token" -default 0]

    return $password_token_matched_p
    
}


ad_proc -public im_get_column_name_of_category {
    -column_name
} {
    Procedure which returns coressponding column name.
    So in case we have project_status_id it returns project_status_name (and opposite)
} {

    set value_to_return ""
    set obj_ctr 0
    set temp_list [list]

    set splitted_column_name [split $column_name "_"]
    set last_one [lindex $splitted_column_name [expr [llength $splitted_column_name] -1]]
    foreach column_part $splitted_column_name {
        if {[expr [llength $splitted_column_name] -1] > $obj_ctr} {
            lappend temp_list $column_part
        }
        incr obj_ctr
    }

    if {$last_one eq "name"} {
        lappend temp_list "id"
    }
    if {$last_one eq "id"} {
        lappend temp_list "name"
    }

    return [join $temp_list "_"]

}

ad_proc -public sencha_folder_permissions {
    -project_id
    -user_id
    {-type "read"}
} {
    Returns allowed folders relative to project

    @param project_id ID of the project for which we check the permission
    @param user_id ID of the user for whom to check permission
    @param type Type of permission requested. Defaults to "read"
    
    @return allowed_folders List of folders (full path) the user is allowed to access
} {

    # Check that the user is allowed within the project
    im_project_permissions $user_id $project_id view read write admin

    if {!$read} {
        # Check if the user maybe is working in a company which has access to the project.
        set company_id [db_string company "select company_id from im_projects where project_id = :project_id"]
        set read [im_biz_object_member_p $user_id $company_id]
        set write 1
    }

    if {!$read} {
        # Check if the user is a freelancer in the project
        # Do this by checking the assignments
        set read [db_string assignment_exists "select 1 from im_freelance_packages fp, im_freelance_assignments fa where fa.freelance_package_id = fp.freelance_package_id and fp.project_id = :project_id and fa.assignee_id = :user_id limit 1" -default 0]

        # Freelancer are never allowed to write directly into folders
        set write 0
    }

    set allowed_folders [list]
    set project_path [im_filestorage_project_path $project_id]

    if {$type eq "read" && $read} {
        if {[im_user_is_pm_p $user_id] || [im_user_is_admin_p $user_id]} {
            # get a list of all the folders in the project

            set folder_files [ad_find_all_files -include_dirs 1 $project_path]
            
            # Remove requested_path from list
            set idx [lsearch $folder_files $project_path]
            set folder_files [lreplace $folder_files $idx $idx]

            foreach file $folder_files {
                if {[file isfile $file]} {
                    set dir_name [file dirname $file]
                } else {
                    set dir_name $file
                }
                if {[lsearch allowed_folders $dir_name]<0} {
                    lappend allowed_folders $dir_name
                }
            }
        } else {
            if {[im_user_is_customer_p $user_id]} {
                set allowed_folders_param  [parameter::get_from_package_key -package_key "sencha-portal" -parameter "CustomersFolder" -default ""]
            } 
            if {[im_user_is_freelance_p $user_id]} {
                set allowed_folders_param  [parameter::get_from_package_key -package_key "sencha-portal" -parameter "FreelancersFolder" -default ""]
            }
            
            foreach allowed_folder_par $allowed_folders_param {
                set allowed_folder "${project_path}/$allowed_folder_par"

                # Avoice duplicates
                if {[lsearch $allowed_folders $allowed_folder]<0} {
                    lappend allowed_folders $allowed_folder
                } 
            }
        }
    }

    # Handle write folders
    if {$type eq "write" && $write} {
        if {[im_user_is_customer_p $user_id]} {
            set allowed_folders_param  [parameter::get_from_package_key -package_key "sencha-portal" -parameter "CustomersFolder" -default ""]
            set not_allowed_write [list "Final"]
            foreach allowed_folder_par $allowed_folders_param {
                if {[lsearch $not_allowed_write $allowed_folder_par]<0} {
                    set allowed_folder "${project_path}/$allowed_folder_par"
                    # Avoice duplicates
                    if {[lsearch $allowed_folders $allowed_folder]<0} {
                        lappend allowed_folders $allowed_folder
                    }
                }
            } 
        }
        
        if {[im_user_is_pm_p $user_id] || [im_user_is_admin_p $user_id]} {
            # get a list of all the folders in the project

            set folder_files [ad_find_all_files -include_dirs 1 $project_path]
            
            # Remove requested_path from list
            set idx [lsearch $folder_files $project_path]
            set folder_files [lreplace $folder_files $idx $idx]

            foreach file $folder_files {
                if {[file isfile $file]} {
                    set dir_name [file dirname $file]
                } else {
                    set dir_name $file
                }
                if {[lsearch allowed_folders $dir_name]<0} {
                    lappend allowed_folders $dir_name
                }
            }
        }
    }
    return $allowed_folders
}

# Register download procedure
ad_proc sencha_portal_project_download {} { 
    sencha_portal_download "project" 
}

ad_proc sencha_portal_user_download {} { 
    sencha_portal_download "user" 
}

proc sencha_portal_download { folder_type } {
    
    set url "[ns_conn url]"

    set path_list [split $url {/}]
    set len [expr [llength $path_list] - 1]

    # Using the group_id as selector for various storage types.
    # skip: +0:/ +1:sencha-portal, +2:download, +3:folder_type, +4:<object_id>, +5:...
    set group_id [lindex $path_list 4]

     # Start retreiving the path starting at:
    set start_index 5

    set file_comps [lrange $path_list $start_index $len]
    set file_name [join $file_comps "/"]


    set base_path [im_filestorage_base_path $folder_type $group_id]

    if {"" == $base_path} {
        return
    }

    set file "$base_path/$file_name"

    catch { set file_readable [file readable $file] }
    
    if ($file_readable) {
        rp_serve_concrete_file $file
    } else {
        return
    }
}
