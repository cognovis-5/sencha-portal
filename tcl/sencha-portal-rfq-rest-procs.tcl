# /packages/sencha-portal-rest-procs

ad_library {
    Rest Procedures for the sencha-portal package
    @author michaldrn@wp.pl
}


ad_proc -public im_rest_get_custom_sencha_rfq {
    { -company_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get all RFQ (external_rfq table)
    
    @return id of the returned rfq
    @return rfq_id id of the returned rfq (exactly same as above)
    @return first_names first names used by user during submission process of rfq
    @return last_name last name used by user during submission process of rfq
    @return company_name company_name used by user during submission process of rfq
    @return email email used by user during submission process of rfq. Important one as it is used to check for possible match company & user
    @return telephone telephone used by user during submission process of rfq
    @return cost_center_id cost_center_id cost center of submission (usually set up in wp-admin of wordpress website)
    @return cost_center_name name of cost center, fetched with usage of cost_center_id
    @return quality_level_id id of level of quality used during submission process
    @return project_type_id id of project type selected during submission process of rfq
    @return project_type_name name of project type selected during submission process of rfq
    @return source_language_id source language of potential project
    @return source_language_name source language name of potential project
    @return subject_area_id subject area of potential project
    @return target_language_ids comma seperated string containing target languages of potential project
    @return target_language_names comma seperated string containing target languages names of potential project
    @return comment optional comment used by user during submission process of rfq 
    @return deadline deadline for that rfq.
    @return deadline_formatted same as deadline but using correct format
    @return matched_company_id matched company id, empty if no matched company was found during submission.
    @return matched_user_id matched user id, empty if no matched user was found during submission.
    @return creation_date rfq creation date. Date is returned, but DB holds timestamp here.
    @return creation_date_formatted same as creation date but with nice format
    @return company_mismatch_p result of check if name of company from 'matched_company_id' is same as name provided in RFQ
    @return matched_company_manager_id manager id of matched company. Empty if there is no matched company.
    @return matched_company_manager_name manager name of matched company (first_names + last_name). Empty if no matched company
    @return customer_reference_number optional value used by customers

} {

    set obj_ctr 0
    set komma ",\n"
    set data [list]
    
    set additional_where ""
    if {$company_id ne ""} {
        set additional_where " where matched_company_id =:company_id"
    }

    set external_rfq_sql "select * from external_rfq $additional_where "

    db_foreach row $external_rfq_sql {
        
        set project_type_name [im_category_from_id $project_type_id]
        set source_language_name [im_category_from_id $source_language_id]
        set target_language_names [list]

        foreach target_lang_id $target_language_ids {
            lappend target_language_names [im_category_from_id $target_lang_id]
        }

        set target_language_names [join $target_language_names ","]

        set company_mismatch_p 0
        # Company mismatch check
        if {$matched_user_id ne ""} {
            set matched_user_name [im_name_from_user_id_helper $matched_user_id]
            set matched_company_name ""
            set matched_user_primary_company [db_0or1row matched_user_primary_company_sql "select company_name as matched_company_name from im_companies where primary_contact_id =:matched_user_id limit 1"]
            if {$company_name ne "" && $company_name ne $matched_company_name} {
                 set company_mismatch_p 1
            }
        }

        # manager_id and manager name
        set matched_company_manager_id 0
        set matched_company_manager_name ""
        if {$matched_company_id ne ""} {
            set matched_company_manager_id [db_string manager_id_sql "select manager_id from im_companies where company_id = :matched_company_id" -default ""]
            if {$matched_company_manager_id ne ""} {
                set matched_company_manager_name [im_name_from_user_id_helper $matched_company_manager_id]
            }
        }

        set date_from_timestamp [string range $creation_date 0 9]
        set cost_center_name ""
        if {$cost_center_id ne ""} {
            set cost_center_name [im_name_from_id $cost_center_id]
        }
        set id $rfq_id
        set comment [im_quotejson $comment]

        # In case 'company_name' is empty + 'matched_company_id' is not empty + there is no company mismatch we can set 'company_name' to matched_company_id name
        if {$company_name eq "" && $matched_company_id ne "" && $company_mismatch_p ne 1} {
            db_0or1row matched_company_name_sql "select company_name from im_companies where company_id =:matched_company_id"
        }
        
        set deadline_formatted [lc_time_fmt $deadline "%d.%m.%Y"]
        set creation_date_formatted [lc_time_fmt $creation_date "%d.%m.%Y"]

        lappend data [im_rest_json_object -proc_name im_rest_get_custom_sencha_rfq]
        incr obj_ctr

    }

    set data [join $data ",\n"]

    set result "{\"success\": 1, \"total\":$obj_ctr, \n\"data\": \[\n$data\n\]}"
    im_rest_doc_return 200 "application/json" $result
    return
    
}

ad_proc -public im_rest_get_custom_sencha_create_project_from_rfq {
    -rfq_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call which turns Request From Quote into ]project-open[ project_id

    @param rfq_id ID of the RFQ (external_rfq table) from which we want to generate project_id

    @return new_project_id ID of newly created project
    @return company_mismatch_p result of check if name of company from 'matched_company_id' is same as name provided in RFQ
} {
        set error ""
        set company_mismatch_p 0
        set success false
        set user_id ""
        set project_id ""
        set modal_title "Creation of project failed"

        # Check if rfq is not blocked
        # Default value is '-1', which means RFQ is not existing at all
        # '1' means RFQ is curently blocked (status 1 - in_creation)
        # Empty string or '0' means rfq exist and we are ready to generate project from it
        set rfq_usage_possible [db_string rfq_usage_possible_sql "select status from external_rfq where rfq_id =:rfq_id" -default -1]

        if {$rfq_usage_possible ne "" && $rfq_usage_possible ne 0} {

            # In case such rfq do not exists anymore
            if {$rfq_usage_possible == -1} {
                set error "RFQ id:$rfq_id does not exist anymore"
                sencha_errors::add_error -object_id $rfq_id -problem $error 
                sencha_errors::display_errors_and_warnings -object_id $rfq_id -modal_title $modal_title -action_type "refresh"
            }
            
            # In case rfq status is 1 (in_creation)
            if {$rfq_usage_possible == 1} {
                set error "Another user is trying to create project from rfq: $rfq_id"
                sencha_errors::add_error -object_id $rfq_id -problem $error 
                sencha_errors::display_errors_and_warnings -object_id $rfq_id -modal_title $modal_title -action_type "refresh"
            }

        } else {

            # First thing to do is setting 'in_creation' status to 1. That will block this rfq for other users
            set in_creation_status 1
            db_dml update_rfq_status "update external_rfq set status =:in_creation_status where rfq_id =:rfq_id"

            # getting rfq data from db
            set rfq_data [db_0or1row existing_rfq "select * from external_rfq where rfq_id =:rfq_id"]  
  
            # getting related rfq files
            set uploaded_filenames_sql "select filename from external_rfq_files where rfq_id =:rfq_id"
    
            # building list of uploaded files for later usage
            set uploaded_filenames [list]
            db_foreach rfq_file $uploaded_filenames_sql {
                lappend uploaded_filenames $filename
            }

            # check if we have matched user
            if {$matched_user_id eq ""} {
                set user_exists_p 0
            } else {
                set matched_company_name [db_string matched_user_primary_company_sql "select company_name as matched_company_name from im_companies where primary_contact_id =:matched_user_id limit 1" -default ""]
                if {$company_name ne "" && $company_name ne $matched_company_name} {
                    set company_mismatch_p 1
                }
                set user_exists_p 1
            }
        
            # check if we have matched company
            if {$matched_company_id eq ""} {
                set company_exists_p 0
            } else {
                # First of all we need to check if such company REALLY exist (it could have been deleted in meantime)
                set matched_company_exists_p [db_0or1row matched_company_exists_sql "select company_id from im_companies where company_id =:matched_company_id"]
                if {$matched_company_exists_p eq 1} {
                    set company_exists_p 1
                    set matched_company_manager_id [db_string no_manager_p_sql "select manager_id from im_companies where company_id = :matched_company_id" -default ""]
                    if {$matched_company_manager_id ne ""} {
                        # If no manager exist, we add currently logged user as one
                        db_dml matched_user_id "update im_companies set manager_id =:rest_user_id where company_id =:matched_company_id"
                    } 
                    # Currently logged should also become Key Account for matched company
                    set key_account_role_id [im_biz_object_role_key_account]
                    set new_rel_id [im_biz_object_add_role $rest_user_id $matched_company_id $key_account_role_id]
                } else {
                    set company_exists_p 0
                }
            }
        
            # in case such user already exsit in our system
            if {$user_exists_p} {
                set existing_user [db_0or1row existing_user_sql "select user_id from cc_users where user_id =:matched_user_id"]
                # making sure this is registered user
                set registered_users [db_string registered_users "select object_id from acs_magic_objects where name='registered_users'"]
                set reg_users_rel_exists_p [db_string member_of_reg_users "
                    select  count(*)
                    from    group_member_map m, membership_rels mr
                    where   m.member_id = :user_id
                        and m.group_id = :registered_users
                        and m.rel_id = mr.rel_id
                        and m.container_id = m.group_id
                        and m.rel_type::text = 'membership_rel'::text
               "]
                if {!$reg_users_rel_exists_p} {
                    relation_add -member_state "approved" "membership_rel" $registered_users $user_id
                }
        
            # in case we do not have such user in our system
            } else {
                set lowercased_email [string tolower $email]
                set username $lowercased_email
                # set screen_name "$first_names $last_name"
                set screen_name ""
                set password [ad_generate_random_string]
                set password_confirm $password

                # now we "double check" for email in our database
                set user_id [db_string existing_user_email_sql "select user_id from cc_users where email =:lowercased_email" -default ""]
            
                # no matched user id and also no such email in our database, we can create new user
                if {$user_id eq ""} {
                    array set creation_info [auth::create_user \
                             -username $username \
                             -email $email \
                             -first_names $first_names \
                             -last_name $last_name \
                             -screen_name $screen_name \
                             -password $password \
                             -password_confirm $password_confirm \
                            ]
                    set user_id $creation_info(user_id)
                    ns_log Notice "Created [array get creation_info]"
                }
        
                # We are also updating matched user id in external_rfq table in case we will get errors at later stage 
                # (no errors means this rfq will be deleted)
                db_dml matched_user_id "update external_rfq set matched_company_id =:user_id where rfq_id =:rfq_id"
            }

        # Add the user to the customer profile
        im_profile::add_member -profile "Customers" -user_id $user_id

            # in case such company exist
            if {$company_exists_p} {
                set existing_company [db_0or1row existing_company_sql "select * from im_companies where company_id =:matched_company_id"]
            # in case we do not have such company in our system
            } else {
                set company_type_id [im_company_type_customer]
                set company_status_id [im_company_status_active]

                # take care of situation when company_name is empty
                if {$company_name eq ""} {
                    set company_name "$first_names $last_name"
                }
 
                set company_id [im_company::new \
                    -company_name       $company_name \
                    -company_type_id    $company_type_id \
                    -company_status_id  $company_status_id \
                    -no_callback ]
     
                # setting new user as accounting contact & primary_contact + adding him as company member
                db_dml update_primary_contact "update im_companies set accounting_contact_id = :user_id, primary_contact_id = :user_id where company_id = :company_id"
                set role_id [im_biz_object_role_full_member]
                im_biz_object_add_role $user_id $company_id $role_id

                db_dml matched_user_id "update im_companies set manager_id =:rest_user_id where company_id =:company_id"

                # Currently logged should also become Key Account for matched company
                set key_account_role_id [im_biz_object_role_key_account]
                set new_rel_id [im_biz_object_add_role $rest_user_id $company_id $key_account_role_id]

                # We are also updating matched user id in external_rfq table in case we will get errors at later stage 
                # (no errors means this rfq will be deleted)
                db_dml matched_company_id "update external_rfq set matched_company_id =:company_id where rfq_id =:rfq_id"

                # default invoice templates, later we can move this to intranet-invoices as callback
                set default_invoice_template_id [db_string invoice_template "select category_id from im_categories
                    where category_type = 'Intranet Cost Template'
                    and aux_int1 = 3700
                    and enabled_p = 't'
                    order by sort_order asc, category_id desc
               limit 1" -default ""]
    
                set default_bill_template_id [db_string invoice_template "select category_id from im_categories
                    where category_type = 'Intranet Cost Template'
                    and aux_int1 = 3704
                    and enabled_p = 't'
                    order by sort_order asc, category_id desc
                limit 1" -default ""]
    
                set default_po_template_id [db_string invoice_template "select category_id from im_categories
                    where category_type = 'Intranet Cost Template'
                    and aux_int1 = 3706
                    and enabled_p = 't'
                    order by sort_order asc, category_id desc
                limit 1" -default ""]
    
                set default_quote_template_id [db_string invoice_template "select category_id from im_categories
                    where category_type = 'Intranet Cost Template'
                    and aux_int1 = 3702
                    and enabled_p = 't'
                    order by sort_order asc, category_id desc
                limit 1" -default ""]

                db_dml update_default_templates "update im_companies
                    set default_invoice_template_id = :default_invoice_template_id,
                    default_bill_template_id = :default_bill_template_id,
                    default_po_template_id = :default_po_template_id,
                    default_quote_template_id = :default_quote_template_id
                    where company_id = :company_id"

            }

            # we can create new project now
            catch {
                set project_id [im_translation_create_project \
                    -company_id $company_id \
                    -project_lead_id $rest_user_id \
                    -source_language_id $source_language_id \
                    -target_language_ids $target_language_ids \
                    -project_type_id $project_type_id \
                    -project_status_id [im_project_status_potential] \
                    -company_contact_id $user_id \
                    -project_source_id [im_project_source_wordpress_plugin] \
                    -no_callback]
            } errmsg 

            if {$project_id eq "" && [string length $errmsg] > 0} {
                # If error occured during project creation, we need to change rfq status back to null (which means it is not 'in_creation' anymore)
                set in_creation_status 0
                db_dml update_rfq_status "update external_rfq set status =:in_creation_status where rfq_id =:rfq_id"
                sencha_errors::add_error -object_id $rfq_id -problem $errmsg
                sencha_errors::display_errors_and_warnings -object_id $rfq_id -modal_title $modal_title -action_type "refresh" 
            } else {
                set success true
            }

            # updating expected quality and project status (it needs to be potential)
            set new_status_id [im_project_status_potential]
            db_dml update_primary_contact_and_status "update im_projects set expected_quality_id = :quality_level_id, project_status_id = :new_status_id where project_id = :project_id"
        
            # creating a project note from rfq comment
        db_dml update_project_note "update im_projects set note = :comment where project_id = :project_id"
        
        if {$comment ne ""} {
        set note [string trim $comment]
        
        set note [template::util::richtext::create $note "text/html" ]
        set note_type_id [im_note_type_project_info]
        
        set note_id [db_exec_plsql create_note "
            SELECT im_note__new(
                NULL,
                'im_note',
                now(),
                :user_id,
                '[ad_conn peeraddr]',
                null,
                :note,
                :project_id,
                :note_type_id,
                [im_note_status_active]
            )
        "]
        }
            # callback!
            cog::callback::invoke -object_type "im_project" -object_id $project_id -type_id $project_type_id -status_id $new_status_id -action after_create

            # make sure we have skills
            if {[apm_package_installed_p "intranet-freelance"]} {
                im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
                im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_expected_quality] -skill_ids $quality_level_id
                foreach target_language_id $target_language_ids {
                    im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $target_language_id
                }
            }

            # dealing with uploaded files (using external_rfq_table for that purpose)
            if {[exists_and_not_null uploaded_filenames]} {

                set uploaded_files_original $uploaded_filenames
                set source_folder [im_trans_task_folder -project_id $project_id -folder_type source]
                set project_dir [im_filestorage_project_path $project_id]
                set source_dir "${project_dir}/$source_folder"
                file mkdir "${project_dir}/$source_folder"

                set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]
                set external_uploads_folder "$base_path_unix/external_rfq"
                foreach file $uploaded_filenames {
                    set original_filename [db_string get_original_filename "select original_filename from external_rfq_files where filename = :file" -default $file]
                    file rename -force "$external_uploads_folder/$file" "${source_dir}/$original_filename"
                }
           
                # callbacks to MemoQ & Trados
                callback im_project_after_file_upload -project_id $project_id -type_id $project_type_id -status_id [im_project_status_potential]

                foreach file $uploaded_files_original {
                     set task_filename $file
                     set task_uom_id [im_uom_s_word] 
                     set task_filename [db_string get_original_filename "select original_filename from external_rfq_files where filename = :file" -default $file]
                     set count_created_tasks [db_string count_created_tasks "select count(*) from im_trans_tasks where project_id = :project_id and task_filename =:task_filename " -default 0]
                     if {$count_created_tasks eq 0} {
                        im_task_insert $project_id $task_filename $task_filename 0 $task_uom_id $project_type_id $target_language_ids
                     }
                }
                # not sure if we need the one below
                # set create_invoice_p [db_string tasks_exist "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]
            }

            # updating project dates
            im_translation_update_project_dates -project_id $project_id

            # In case deadline was provided during RFQ submission, we use that date
            if {$deadline ne ""} {
            set deadline_in_the_past_p [db_string deadline "select 1 from dual where :deadline < now()" -default 0]
            if {!$deadline_in_the_past_p} {
            db_dml update_project "update im_projects set end_date = :deadline where project_id = :project_id"
            }
            }

            # remove RFQ from db
            if {$project_id ne ""} {
                db_transaction {
                    db_dml delete_rfq "delete from external_rfq_files where rfq_id =:rfq_id" 
                    db_dml delete_rfq "delete from external_rfq where rfq_id =:rfq_id" 
                }
            }

            set result "{\"success\":true, \"error\": \"$error\", \"new_project_id\":$project_id, \"company_mismatch_p\":$company_mismatch_p}"

            im_rest_doc_return 200 "application/json" $result
            return

        }
}


ad_proc -public im_rest_get_custom_sencha_find_duplicate {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to find a duplicate
} {
    
    set result "{\"success\":3, \"error\": \"\",\n\"data\": \[\n\n\]}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_rest_get_custom_sencha_create_rfq {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to create RFQ from contact form in Wordpress
} {
    
    set success 1
    set error ""

    array set query_hash $query_hash_pairs
    

    if {[info exists query_hash(first_name)]} {
        set first_names $query_hash(first_name)
    } else {
        set first_names ""
    }

    if {[info exists query_hash(last_name)]} {
        set last_name $query_hash(last_name)
    } else {
        set last_name ""
    }

    if {[info exists query_hash(cost_center_id)]} {
        set cost_center_id $query_hash(cost_center_id)
    } else {
        set cost_center_id 0
    }

    if {[info exists query_hash(company_name)]} {
        set company_name $query_hash(company_name)
    } else {
        set company_name ""
    }

    if {[info exists query_hash(email)]} {
        set email $query_hash(email)
    } else {
        set email ""
    }

    if {[info exists query_hash(telephone)]} {
        set telephone $query_hash(telephone)
    } else {
        set telephone ""
    }

    if {[info exists query_hash(quality_level)]} {
        set quality_level_id $query_hash(quality_level)
    } else {
        set quality_level_id ""
    }

    if {[info exists query_hash(project_type)]} {
        set project_type_id $query_hash(project_type)
    } else {
        set project_type_id ""
    }

    if {[info exists query_hash(source_language)]} {
        set source_language_id $query_hash(source_language)
    } else {
        set source_language_id ""
    }

    if {[info exists query_hash(uploaded_filename)]} {
        set uploaded_filename $query_hash(uploaded_filename)
    } else {
        set uploaded_filename ""
    }

    if {[info exists query_hash(comment)]} {
        set comment $query_hash(comment)
    } else {
        set comment ""
    }
    

    if {[info exists query_hash(target_languages)]} {
        set target_language_ids [list]
        set target_languages $query_hash(target_languages)
        foreach selected_target_language_id [split $target_languages ","] {
            lappend target_language_ids $selected_target_language_id
        }
    } else {
        set target_language_ids ""
    }


    set matched_user_id [db_string first_last_name_exists_p "
        select  user_id
        from    cc_users
        where   lower(trim(first_names)) = lower(trim(:first_names)) and
            lower(trim(last_name)) = lower(trim(:last_name)) and
            lower(trim(email)) = lower(trim(:email))
        " -default ""]

    set matched_company_id ""
    if {$company_name ne ""} {
        set matched_company_id [db_string company_exists_p "
            select  company_id
            from    im_companies
            where   lower(trim(company_name)) = lower(trim(:company_name))
            " -default ""]
            
            regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path
            if {$matched_company_id eq ""} {
                set matched_company_id [db_string company_exists "select company_id from im_companies where company_path = :company_path" -default ""]
            }
    }


    set current_date [clock format [clock seconds] -format "%Y-%m-%d"]
    
    set new_rfq_id [db_string insert_rfq "select external_rfq__new(:first_names, :last_name, :cost_center_id, :company_name, :email, :telephone, :quality_level_id, :project_type_id, :source_language_id, :target_language_ids, :matched_user_id, :matched_company_id, :comment, :current_date)" -default 0]

    set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]
    set rfq_files_folder "$base_path_unix/external_rfq"
    
    foreach uploaded_file [split $uploaded_filename ","] {
        set file_data [split $uploaded_file, "@"]
        set filename [lindex $file_data 0]
        set original_filename [lindex $file_data 1]
        set extension [lindex $file_data 2]
        # quickfix (to be changed later) remove comma
        regsub -all {,} $extension {} extension
        set size [file size "$rfq_files_folder/$filename"]
        set res [db_string insert_rfq_file  "select external_rfq_file__new(:new_rfq_id, :filename, :original_filename, :extension, :size)" -default 0]
    }

    set result "{\"success\":$new_rfq_id , \"error\": \"$error\",\n\"data\": \[\n\n\]}"
    im_rest_doc_return 200 "application/json" $result
    return
}



ad_proc -public im_rest_get_custom_sencha_delete_rfq {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to delete spam RFQ's
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(rfq_id)]} {
        set rfq_id $query_hash(rfq_id)
        set result true
        set error ""
    } else {
        set result false
        set error "You must provide rfq id"
    }    
    
    db_transaction {
        db_dml delete_rfq "delete from external_rfq_files where rfq_id =:rfq_id" 
        db_dml delete_rfq "delete from external_rfq where rfq_id =:rfq_id" 
    }

    if {$result} {
        set result "{\"success\":true}"
    } else {
        set result "{\"success\":true, \"error\":\"$error\"}"
    }
    
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_link_rfq_to_user {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to delete spam RFQ's
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(rfq_id)]} {
        set rfq_id $query_hash(rfq_id)
        set rfq_id_error ""
    } else {
        set rfq_id_error "You must provide rfq id"
    }   

    if {[info exists query_hash(matched_user_id)]} {
        set matched_user_id $query_hash(matched_user_id)
        set matched_user_id_error ""
    } else {
        set matched_user_id_error "You must provide user id"
    }   

    if {[info exists query_hash(action)]} {
        set action $query_hash(action)
        set action_error ""
    } else {
        set action_error "You must provide action"
    }   

    if {$matched_user_id_error eq "" && $rfq_id_error eq ""} {
        set result true
    } else {
        set result false
    }

    if {$result} {
        if {$action eq "unlink"} {
            db_dml update_matched_user_id "update external_rfq set matched_user_id = null where rfq_id = :rfq_id"
        } else {
            db_dml update_matched_user_id "update external_rfq set matched_user_id =:matched_user_id where rfq_id = :rfq_id"
        }
    }
    
    set result "{\"success\": $result}"
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_get_rfq_files {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get uploaded files for rfq
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(rfq_id)]} {
        set rfq_id $query_hash(rfq_id)
        set result true
        set error ""
    } else {
        set result false
        set error "You must provide rfq id"
    }    

    set obj_ctr 0
    set komma ",\n"
    set data ""
   
    set external_rfq_files_sql "select * from external_rfq_files where rfq_id = :rfq_id"

    db_foreach row $external_rfq_files_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append data "$komma{\"rfq_id\": \"$rfq_id\", \"rfq_file_id\": \"$rfq_file_id\", \"filename\":\"$filename\", \"original_filename\":\"$original_filename\", \"extension\":\"$extension\", \"size\":\"$size\"}"
        incr obj_ctr
    }

    set result "{\"success\": 1, \"error\": \"0\",\n\"data\": \[\n$data\n\]}"
    im_rest_doc_return 200 "application/json" $result
    return
    

}



ad_proc -public im_rest_get_custom_sencha_get_similar_users {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get similar users 
    (Biz-card way)
} {

    array set query_hash $query_hash_pairs

    if {[info exists query_hash(rfq_id)]} {
        set rfq_id $query_hash(rfq_id)
        set result true
        set error ""
    } else {
        set result false
        set error "You must provide rfq id"
    }    

    set obj_ctr 0
    set komma ",\n"
    set data ""


    db_1row external_rfq "select * from external_rfq where rfq_id = :rfq_id"

    set currently_matched_user_id $matched_user_id

    set org_first_names [string trim $first_names]
    set org_last_name [string trim $last_name]
    set org_email [string tolower [string trim $email]]
    set org_company_name [string trim $company_name]

    set first_names [string tolower [string trim $first_names]]
    set last_name [string tolower [string trim $last_name]]
    set email [string tolower [string trim $email]]
    set company_name [string tolower [string trim $company_name]]

    set q_list [list]
    set contact_or_query [list]
    set company_or_query [list]
    if {"" != $first_names} { 
    foreach t $first_names { lappend q_list $t }
        lappend contact_or_query "lower(u.first_names) like '%$first_names%'"
    }
    if {"" != $last_name} { 
    foreach t $last_name { lappend q_list $t }
        lappend contact_or_query "lower(u.last_name) like '%$last_name%'"
    }
    if {"" != $email} { 
    foreach t $email { lappend q_list $t }
        lappend contact_or_query "lower(u.email) like '%$email%'"
    }
    if {"" != $company_name} { 
    foreach t $company_name { lappend q_list $t }
        lappend company_or_query "lower(c.company_name) like '%$company_name%'"
    }
    
    # Cleanup invalid chars
    set q_list_clean [list]
    foreach q_element $q_list {
    regsub -all {\&} $q_element {} q_element
    if {$q_element ne ""} {
        lappend q_list_clean $q_element
    }
    }

    set q [join $q_list_clean " | "]
   
    set contact_or_clause [join $contact_or_query "\n\t\t\tOR " ]
    if {"" == $contact_or_clause} { set contact_or_clause "1=0" }

    set company_or_clause [join $company_or_query "\n\t\t\tOR " ]
    if {"" == $company_or_clause} { set company_or_clause "1=0" }

    set similar_users_sql "
            select  u.user_id,
                1 as rank
            from    cc_users u
            where   $contact_or_clause
            UNION
            select  so.object_id as user_id,
                        (rank(so.fti, :q::tsquery) * sot.rel_weight)::numeric(12,2) as rank
            from    im_search_objects so,
                im_search_object_types sot
            where   so.object_type_id = sot.object_type_id and
                so.fti @@ to_tsquery('default',:q)
    "


    db_foreach row $similar_users_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        set user_exist [db_0or1row get_user "select * from cc_users where person_id = :user_id"]
        if {$user_exist ne 0} {
            set matched false
            if {$currently_matched_user_id eq $user_id} {
                set matched true
            }
            append data "$komma{\"object_id\":$user_id, \"user_id\":$user_id, \"first_name\":\"$first_names\", \"last_name\":\"$last_name\", \"email\":\"$email\", \"matched\":$matched}"
            incr obj_ctr
        }
    }


    set result "{\"success\": 1, \"error\": \"$error\",\n\"data\": \[\n$data\n\]}"
    im_rest_doc_return 200 "application/json" $result
    return

}

ad_proc -public intranet_rest::get::download_rfq_file {
    -rfq_file_id:required
} {

    @author malte.sussdorff@cognovis.de
    @creation_date 2020-07-28

    @param rfq_file_id integer ID of the rfq_file to download

    @return file Return the file
    
} {

    set filename [db_string filename "select filename from external_rfq_files where rfq_file_id =:rfq_file_id" -default ""]

    if {$filename eq ""} {
        return [cog_rest::error -http_status 400 -message "File for $rfq_file_id not found"]
    }

    set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]
    set external_uploads_folder "$base_path_unix/external_rfq"
    set file "$external_uploads_folder/$filename"

    if [file readable $file] {
        set guessed_file_type [ns_guesstype $file]
        set outputheaders [ns_conn outputheaders]
        set original_filename [db_string filename "select original_filename from external_rfq_files where rfq_file_id =:rfq_file_id" -default ""]
        if {$original_filename eq ""} {
            set original_filename $filename
        }
        ns_set cput $outputheaders "Content-Disposition" "attachment; filename=$original_filename"
        ns_returnfile 200 $guessed_file_type $file
    } else {
        return [cog_rest::error -http_status 400 -message "File for $rfq_file_id not found"]
    }
}
        