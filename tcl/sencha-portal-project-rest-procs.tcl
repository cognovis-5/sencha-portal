# /packages/sencha-portal-rest-procs

ad_library {
    Rest Procedures for the sencha-portal package - project related
    @author michaldrn@wp.pl
}


ad_proc -public intranet_rest::get::project_trans_list {
    { -project_id ""}
    { -project_status_id ""}
    { -project_type_id "" }
    { -ignore_subcategories_p 0}
    { -cost_type_id ""}
    { -cost_status_id ""}
    { -rest_user_id 0 }
    { -limit "100" }
    { -start "" } 
} {
     Handler for GET rest calls which returns projects

    @param project_id integer Return just the project_id
    @param project_status_id integer category "Intranet Project Status" Limit to projects of a certain status. Exclude deleted status by default.
    @param project_type_id integer category "Intranet Project Type" Limit to projects of a certain type. We will by default exclude Task/Ticket (100/101)
    @param ignore_subcategories_p boolean Defaults to 0 should be removed.
    @param cost_type_id integer category "Intranet Cost Type" Cost type for which we get an array. Used in invoicing. Needs removal.
    @param cost_status_id integer category "Intranet Cost Status" Cost status... neeeds to be removed.
    @param limit integer Limit the array to only a certain number of entries
    @param start integer Start with the -start- entry (using OFFSET)

    @return projects
    @return_projects project_id integer ProjectId
    @return_projects project_nr string ProjectNR for the project
    @return_projects project_name string Name of the project
    @return_projects project_status_id integer "Intranet Project Status" Status_id of the project
    @return_projects project_type_id integer "Intranet Project Type" Type_id of the project
    @return_projects project_type_name string I18N of the project type
    @return_projects company_project_nr string Customer Reference Number
    @return_projects end_date string ANSI date for the deadline
    @return_projects end_date_formatted string Formatted deadline as per locale
    @return_projects source_language_id integer source language ID of the project
    @return_projects source_language_name string I18N of the source language
    @return_projects target_language_ids string Comma separated (TCL) List of target languagages
    @return_projects target_languages_names_comma string Comma separate list of i18n target langugages
    @return_projects subject_area_id integer "Intranet Subject Area" Subject Area ID of the project
    @return_projects subject_area_name string Name of the subject area (I18N)
    @return_projects project_lead_id integer ID of the PM running the project
    @return_projects project_lead_name string Name of the PM
    @return_projects company_contact_id integer ID of the company contact for this project
    @return_projects company_contact_name string Name of the company contact
    @return_projects company_contact_email string E-Mail address for the company contact
    @return_projects cost_quotes_cache string Cached quotes of the project
    @return_projects processing_time integer Time the project usually takes to finish in days
    @return_projects project_currency string Currency of the project
    @return_projects delivery_files_p integer (boolean) Does this project have files in the delivery folder

} {

    set additional_wheres [list]
    
    if {$project_status_id ne ""} {


        # Switch is only needed until we clean up the projects list. and then it should be project_status_ids....
        switch $project_status_id {
            71 {
                if {$ignore_subcategories_p} {
                    lappend additional_wheres "project_status_id = :project_status_id"
                } else {
                    # We query this for all potential projects. Should exclude those in Status 72 though
                    set project_statuses_subcategories_ids [im_sub_categories $project_status_id]
                    set idx [lsearch $project_statuses_subcategories_ids "72"]
                    set project_statuses_subcategories_ids [lreplace $project_statuses_subcategories_ids $idx $idx]
                    lappend additional_wheres "project_status_id in ([template::util::tcl_to_sql_list $project_statuses_subcategories_ids])"
                }
            }
            72 {
                # we call this to get the requesting projects. only query those in this status
                lappend additional_wheres "project_status_id = :project_status_id"
            }
            default {
                if {$ignore_subcategories_p} {
                    lappend additional_wheres "project_status_id = :project_status_id"
                } else {
                    set project_statuses_subcategories_ids [im_sub_categories $project_status_id]

                    if {[llength $project_statuses_subcategories_ids]>0} {
                        lappend additional_wheres "project_status_id in ([template::util::tcl_to_sql_list $project_statuses_subcategories_ids])"
                    }
                }
            }
        }
    } else {
        # Exclude closed projects
        lappend additional_wheres "project_status_id not in ([template::util::tcl_to_sql_list [im_sub_categories [im_project_status_closed]]]) "
    }

    if {$project_type_id ne ""} {
        lappend additional_wheres "project_type_id = :project_type_id"
    } else {
        # Exclude tasks and tickets
        lappend additional_wheres "project_type_id not in (100,101)"
    }

    if {$project_id ne ""} {
        lappend additional_wheres "project_id =:project_id"
    }

    # Check if user is pm and limit projects
    if {![im_user_is_pm_p $rest_user_id]&&![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {

        # Check for company. then display projects of the company
        if {[im_user_is_customer_p $rest_user_id]} {
            set contact_company_id [im_user_main_company_id -user_id $rest_user_id]
            lappend additional_wheres "company_id =:contact_company_id"
        } else {
            # Do not return anything.
            lappend additional_wheres "0 = 1"
        }

        # Check for freelancers. Eventually... when we need it....

    } 
    
    # Order by creation_date and limit to 50 entries each for the time being
    
    set projects_sql "select * from im_projects where [join $additional_wheres " and "] order by end_date"
    set projects [list]

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    if {$limit ne ""} {
	    append projects_sql "\nLIMIT $limit\n"
    }

    if {$start ne ""} {
	    append projects_sql "OFFSET $start\n"
    }

    db_foreach project $projects_sql {

            set target_languages_ids [im_target_language_ids $project_id]
            set target_languages_names_as_list [list]
            foreach target_lang_id $target_languages_ids {
                lappend target_languages_names_as_list [im_name_from_id $target_lang_id]
            }
            set target_languages_names_comma [join $target_languages_names_as_list ","]
            set target_language_ids [join $target_languages_ids ","]
           
            set project_type_name [im_name_from_id $project_type_id]
            set source_language_name ""
            if {$source_language_id ne ""} {
                set source_language_name [im_name_from_id $source_language_id]
            }
            set subject_area_name ""
            if {$subject_area_id ne ""} {
                set subject_area_name [im_name_from_id $subject_area_id]
            }

            set end_date_formatted [lc_time_fmt $end_date "%d.%m.%Y"]
            set project_currency $project_budget_currency


            
            if {$cost_type_id ne ""} {
                set project_financial_document_list [intranet_rest::get::project_finance_documents -project_id $project_id -cost_type_id $cost_type_id -cost_status_id $cost_status_id -data_only_p 1]

                if {[llength $project_financial_document_list]>0} {
                    set project_financial_document [im_rest_json_object_from_list -objects $project_financial_document_list]
                    set financial_documents "\[\n$project_financial_document\n\]"

                    # Calculate the costs based on actual numbers
                    set cost_quotes_calculated 0
                    foreach cost $project_financial_document_list {
                        if {[lindex $cost 23] ne ""} {
                            set cost_quotes_calculated [expr $cost_quotes_calculated + [lindex $cost 23]]
                        }
                    }

                    if {$cost_quotes_calculated ne 0} {
                        set cost_quotes_cache $cost_quotes_calculated
                    }
                } else {
                    set financial_documents ""
                }
            } else {
                set financial_documents ""
            }

            

            # Check for delivery files
            set project_delivery_files_list [im_rest_get_custom_sencha_project_delivery_files -project_id $project_id -data_only_p 1]
            if {[llength $project_delivery_files_list] > 0} {
               set delivery_files_p 1 
            } else {
                set delivery_files_p 0
            }

            if {$project_lead_id ne "" && $project_lead_id ne 0} {
                set project_lead_name [im_name_from_id $project_lead_id]
            } else {
                set project_lead_name ""
            }
            
            if {$company_contact_id eq ""} {
                # Set to the primary contact of the company
                set company_contact_id [db_string primary_contact "select primary_contact_id from im_companies where company_id = :company_id" -default ""]
            }

            if {$company_contact_id eq ""} {
                # Default to rest user. Something is really off with this project
                set company_contact_id $rest_user_id
            }                  

            set company_contact_name [im_name_from_id $company_contact_id]
            set company_contact_email [im_email_from_user_id $company_contact_id]
            
            lappend projects [cog_rest::json_object]
            
        }

        return [cog_rest::return_array]
}


ad_proc -public intranet_rest::get::sencha_project_action {
    -project_id
    -action_type
    { -rest_user_id 0 }
} {


     Endpoint providing handlers for multiple action_types on projects.
     Mainly used in Portals

     @param project_id integer of project which we want to update
     @param action_type string currently accept/deny

     @return data Object with result of action

     @return_data project_id integer id of project for which we executed action
     @return_data action_done string action type which we just executed
     @return_data message string message returned
     @return_data error string error returned. "no error" if action was a success


} {
    
    set success false
    set error "no error"
    set message ""

    # Getting locale
    set locale [lang::user::locale -user_id $rest_user_id]

    # First we check if such project exists
    set project_exist_p [db_string project_exist_p "select 1 from im_projects where project_id=:project_id" -default 0]

    if {$project_exist_p > 0} {

        switch $action_type {
            "accept" {
                set data [list project_id $project_id]
                im_rest_get_custom_sencha_qto -query_hash_pairs $data -action_only_p 1
                set success true
                set action_done "accepted project"   
                set message [lang::message::lookup $locale sencha-portal.project_accepted]
            }
            "deny" {
               set status_rejected_id [im_cost_status_rejected]
               db_dml update_project "update im_projects set project_status_id =:status_rejected_id where project_id=:project_id"
               set success true
               set action_done "rejected project"   
               set message [lang::message::lookup $locale sencha-portal.project_denied]  
            }
        }

    } else {
        set error "Project with $project_id not found"
    }

    return [cog_rest::json_object]

}


ad_proc -public im_rest_get_custom_trans_project_permission {
    -project_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
     Endpoint is checking if user has permission to view project 
     Used in Project Manager App

     This procedure is currently NOT used

} {

    set user_id $rest_user_id

    set user_has_permission_p 0

    # Global Admin ?
    set site_wide_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
    if {$site_wide_admin_p} {
        set user_has_permission_p 1
    } else {
        set user_has_permission_p [im_project_permissions $user_id $project_id view read write admin]
    }
    
    if {$user_has_permission_p ne 0} {
        db_1row select_project  "select * from im_projects where project_id = :project_id"
        set result "{\"success\": true, \"user_id\":$user_id, \"project_id\":$project_id, \"project_nr\":\"$project_nr\", \"project_name\":\"$project_name\"}"
    } else {
        set result "{\"success\": false, \"user_id\":$user_id}"
    }
    
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public intranet_rest::get::sencha_project_delivery_files {
    -project_id:required
    { -data_only_p 0}
    { -format "json" }
    -rest_user_id:required
} {
    Handler for GET rest calls to get list of finished files (toghether with filepaths) of given project_id

    @param project_id object im_project::read project_id of project for which one we need list of completed files

    @return data json_array Array of files in the delivery folder
    @return_data file_path string path of file
    @return_data file_type string extension of file
    @return_data file_name string name of file

} {
    
    set data [list]
    set delivery_files_folder_name [parameter::get_from_package_key -package_key "sencha-portal" -parameter "DeliveryFolder" -default "Final"]

    if {$delivery_files_folder_name ne ""} {
         
        set delivery_files_path "[im_filestorage_project_path $project_id]/$delivery_files_folder_name"
        set delivery_files_list [glob -nocomplain -directory "${delivery_files_path}" "*"]
        foreach completed_file $delivery_files_list {
            # We need to split filepath to get filename (last element)
            set exploded_filename [split $completed_file "/"]
            set last_element [expr [llength $exploded_filename] -1]

            set file_name [lindex $exploded_filename $last_element]
            set file_type [file extension $completed_file]

            set download_url "[ad_url][apm_package_url_from_key "intranet-sencha-tables"]download-project-file?project_id=$project_id&file_name=$file_name"

            set file_path $download_url
            
            lappend data [cog_rest::json_object]
        }

    } else {
        cog_rest::error -http_status 500 -message "Folder for delivery files does not exist"
    }

    if {$data_only_p eq 1} {
        return $data
    } else {
        return [cog_rest::return_array]
    }
}


ad_proc -public intranet_rest::get::project_assignments {
    -project_id
    -assignee_id
    { -rest_user_id 0 }
} {
    Endpoint which fetches all assignments for a freelacner fo given project_id and assignee_id
    Token and email are used for veryfication.

    Return is an array of objects.

    @param project_id integer Project for which we fetch the assignments
    @param assignee_id integer Freelancer assigned to the assignments we are getting for the project. Return all assignments for the project when no assignee is given
    
    @return project_assignments Array of freelancers assigned in that project
    @return_project_assignments assignment_id integer ID of the assignment
    @return_project_assignments freelance_package_id integer PackageID for the assignment
    @return_project_assignments freelance_package_name string Name of the Package
    @return_project_assignments freelance_package_file_id integer File of the assignment
    @return_project_assignments rate string Rate at which the freelancer was hired
    @return_project_assignments uom_id integer Units of measure_id for the assignment
    @return_project_assignments uom_name string Name of the UOM for nice display
    @return_project_assignments assignment_units integer Units (of the units of measure)
    @return_project_assignments assignment_status_id integer Status of the assignment. Reference "Intranet Freelance Assignment Status"
    @return_project_assignments assignment_status_name string Category_name for the status (not I18N)
    @return_project_assignments assignment_type_id integer ID of the assignment. Reference "Intranet Trans Task Type"
    @return_project_assignments assignment_type_name string Category name for the type of assignment. Not I18N
    @return_project_assignments assignee_id integer party_id of the assignee
    @return_project_assignments assignee_name string Name of the assignee
    @return_project_assignments task_names string String with task_names, separated by ","
    @return_project_assignments assignment_comment string Comment provided with the assignment.
    @return_project_assignments purchase_order_id integer invoice_id for the purchase_order associated with the assignment
    @return_project_assignments start_date string Date when the assignment could be started earliest
    @return_project_assignments start_date_formatted string Start Date formatted for the local (lc_time_fmt)
    @return_project_assignments end_date Deadline string for the assignment
    @return_project_assignments end_date_formatted string Deadline formatted (lc_time_fmt)
    @return_project_assignments file_present_p integer (boolean) Returns 1 if a package is uploaded by the PM
    @return_project_assignments target_language_id integer Target language for this assignment
    @return_project_assignments target_language_name Name string of the target language (I18N)
    @return_project_assignments pm_reminded_p integer (boolean) Returns 1 if the PM was already reminded to upload a return package
    @return_project_assignments freelancer_rating_possible_p integer (boolean) Checks through im_assignment_is_rating_fl_possible if a rating is possible. If not, return false, otherwise true

} {
    # Allow Admins and pms to see all assignees of a project, remove 'Assiged Other' for PM's
    set additional_status_where_sql ""
    if {$assignee_id eq "" && ([im_is_user_site_wide_or_intranet_admin $rest_user_id] || [im_user_is_pm_p $rest_user_id])} {
        set assignee_sql ""
        set additional_status_where_sql "and fa.assignment_status_id <> 4228"
    } else {
        set assignee_sql "and assignee_id = :assignee_id"
    }

    set assignments_sql "
        select distinct freelance_package_name, assignment_id, coalesce(assignment_units,0) as assignment_units, uom_id, im_name_from_id(uom_id) as uom_name, im_name_from_id(uom_id) as uom, coalesce(rate,0) as rate, assignment_status_id, im_name_from_id(assignment_status_id) as assignment_status_name, assignee_id, im_name_from_id(assignee_id) as assignee_name,
            im_name_from_id(assignment_type_id) as assignment_type, start_date, end_date,
            purchase_order_id, fp.freelance_package_id, assignment_type_id, im_name_from_id(assignment_type_id) as assignment_type_name, assignment_comment, 
        source_language_id, target_language_id, o.creation_date,
        (select array_to_string(array_agg(task_name), ', ') from im_trans_tasks where task_id in (select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = fp.freelance_package_id)) as task_names
        from im_freelance_assignments fa, im_freelance_packages fp, im_materials m, acs_objects o
        where project_id = :project_id and fp.freelance_package_id = fa.freelance_package_id $assignee_sql
        and fa.assignment_status_id <> 4230
        and fa.material_id = m.material_id
        and fa.assignment_id = o.object_id
        $additional_status_where_sql
        order by assignment_status_id, creation_date desc
    "

    set project_lead_id [db_string get_project_manager_id "select project_lead_id from im_projects where project_id=:project_id" -default ""]
    set project_assignments [list]
    set out_file_types [im_sub_categories 600]

    db_foreach assignment $assignments_sql {
        set file_present_p [db_string out_package_exists "select 1 from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id in ([template::util::tcl_to_sql_list $out_file_types]) limit 1" -default 0]

    set freelance_package_file_id ""
        # Figure out deadline of previous stage assignmnet
        set previous_stage_deadline ""
        set previous_stage_assignment_ids [im_assignment_previous_stage_assignment_id -assignment_id $assignment_id]
        if {$previous_stage_assignment_ids ne ""} {
            set previous_stage_deadline [db_string get_previous_stage_deadline "select max(end_date) from im_freelance_assignments where assignment_id in ([template::util::tcl_to_sql_list $previous_stage_assignment_ids])" -default ""]
        }

        set start_date $previous_stage_deadline


    # Find the correct file_id
    switch $assignment_status_id {
        4222 - 4224 - 4229 {
        if {$file_present_p} {
            set freelance_package_file_id [db_string package "select freelance_package_file_id from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id in ([template::util::tcl_to_sql_list $out_file_types]) order by freelance_package_file_id desc limit 1" -default ""]
	    if {$freelance_package_file_id ne "" && $assignment_status_id eq 4222} {
		db_dml update_assignment "update im_freelance_assignments set assignment_status_id = 4229 where assignment_id = :assignment_id"
		set assignment_status_id 4229
	    }
        }
        }
        4225 - 4226 - 4227 {
        # Return package
        set freelance_package_file_id [db_string package "select freelance_package_file_id from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id not in ([template::util::tcl_to_sql_list $out_file_types]) order by freelance_package_file_id desc limit 1" -default ""]
        }
    }

        if {$project_lead_id ne ""} {
            set pm_reminded_p [db_string pm_reminded "select 1 from im_freelance_notifications where object_id = :assignment_id and user_id = :project_lead_id limit 1" -default 0]
        } else {
            set pm_reminded_p 0
        }

        set freelancer_rating_possible_p [assignment_is_rating_fl_possible -assignment_id $assignment_id]

        # denormalize
        set start_date_formatted [lc_time_fmt $start_date "%d.%m.%Y"]
        set end_date_formatted [lc_time_fmt $end_date "%d.%m.%Y"]
        set target_language_name [im_name_from_id $target_language_id]
        
        lappend project_assignments [cog_rest::json_object]
    }

    return [cog_rest::return_array]

}


ad_proc -public im_rest_get_custom_sencha_update_assignment_status {
    -assignment_id
    -action_type
    { -freelance_package_file_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET endpoint which allows to update assignments status (Accept or Deny)

    @param action_type Action to be performed on the assignment - Currently accept, 
    @param assignment_id ID of the assignment
} {

    set success false
    set action_done "none"
    set message "Unknown error"
    set modal_title ""

    set assignee_id $rest_user_id
    set locale [lang::user::locale -user_id $rest_user_id]
    set package_download_url ""
     
    # Get project_id from assignment_id
    set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]

    # Get project information, as those are gonna be useful
    db_1row project_info "select project_nr,project_lead_id from im_projects where project_id = :project_id"

    # Get assignment info, would be useful in both "action_type" cases
    db_1row assignment_info "select fp.freelance_package_id, freelance_package_name, assignment_type_id, rate, uom_id,end_date as deadline, purchase_order_id from im_freelance_assignments fa, im_freelance_packages fp where fp.freelance_package_id = fa.freelance_package_id and assignment_id = :assignment_id"

    # Accept
    switch $action_type {
        "accept" {
            # First we check that the assignment is still available, and no other user has been assigned in meantime
            set total_existing_assignee_id [db_string assigned_p "select count(assignee_id) from im_freelance_assignments where freelance_package_id = :freelance_package_id
                and assignment_type_id = :assignment_type_id
                and assignment_status_id in (4222,4224,4225,4226) 
                and assignee_id != :assignee_id" -default 0]
            if {$total_existing_assignee_id > 0} {
                # Check if the current assignee_id is the assignee for the assignment
                set assignee_p [db_string assigneed_p "select 1 from im_freelance_assignments where assignment_id = :assignment_id and assignee_id = :assignee_id" -default 0]
                if {$assignee_p} {
                    set modal_title "[lang::message::lookup $locale sencha-portal.already_assigned]"
                    set message "[lang::message::lookup $locale sencha-portal.lt_already_assigned]"
                    set success 1
                    set action_done "accepted"
                } else {
                    set modal_title "[lang::message::lookup $locale sencha-portal.creation_of_assignment_failed]"
                    set message "[lang::message::lookup $locale sencha-portal.freelance_package_not_available]"
                    set action_done "rejected"
                    set success 0
                }
                set message $error
            } else {
                # We can now update assignment status to 'Accepted'
                db_dml update "update im_freelance_assignments set assignment_status_id = 4222 where assignment_id = :assignment_id"
                # We also need to update other potential assignments to status 4228 (Other Assignment)
                db_dml update_other "update im_freelance_assignments set assignment_status_id = 4228 where assignment_id in (select assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id
                    and assignment_type_id = :assignment_type_id
                    and assignment_status_id in (4220,4221))"
                
                # Add freelancer as project members of project
                set member_role_id [im_biz_object_role_full_member]
                set project_member_ids [im_biz_object_member_ids $project_id]
                if {[lsearch $project_member_ids $assignee_id]<0} {
                    callback im_before_member_add -user_id $assignee_id -object_id $project_id
                    im_biz_object_add_role $assignee_id  $project_id $member_role_id
                    cog::callback::invoke -object_id $project_id -action "after_update"
                    #set touched_p 1
                }   

                # Get task type
                set task_type [string tolower [im_category_from_id -translate_p 0 $assignment_type_id]]
                set provider_id [db_string select_company {
                    select  c.company_id as provider_id
                    from            acs_rels r,
                            im_companies c
                    where       r.object_id_one = c.company_id
                    and     r.object_id_two = :assignee_id
                    limit 1
                } -default 0]


                # Update the tasks(im_trans_tasks) {task_type}_id columns. E.g. trans_id or edit_id
                set task_ids [db_list tasks_from_assignment "select trans_task_id from im_freelance_packages_trans_tasks fptt, im_freelance_assignments fa where fa.freelance_package_id = fptt.freelance_package_id and fa.assignment_id = :assignment_id and assignment_type_id = :assignment_type_id"]
                foreach task_id $task_ids {
                    db_dml update "update im_trans_tasks set ${task_type}_id = :assignee_id, ${task_type}_end_date = to_date(:deadline,'YYYY-MM-DD HH24:MI') where task_id = $task_id"
                }

                # Create purchase order
                # DISCUSS THESE NEXT LINE
                set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id -user_id $rest_user_id]
                im_invoice_send_invoice_mail -invoice_id $invoice_id -recipient_id $assignee_id -from_addr [party::email -party_id $project_lead_id]

                # Notify the project_lead
                # Check that we have packages ready for the target language
                set target_language_id [db_string target "select target_language_id from im_trans_tasks where task_id = :task_id"]
                set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
                im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id \
                    -message "<a href='$assignments_url'>$project_nr</a> :: $task_type accepted by [im_name_from_id $assignee_id] for [im_category_from_id $target_language_id]" \
                    -severity 2

                # Inform the assignee he can start the work
                # First we check if file was already uploaded
                set package_filepath [db_string get_package_filepath "select file_path from im_freelance_package_files where freelance_package_id=:freelance_package_id" -default ""]
                if {$package_filepath ne ""} {
                    if {[file exists $package_filepath]} {
                        set location [util_current_location]
                        set locale [lang::user::locale -user_id $assignee_id]
                        set internal_company_name [im_name_from_id [im_company_freelance]]
                        set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
                        # Set the assignment status to work ready (4229)
                        db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4229 where assignment_id = :assignment_id"

                        set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
                        set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]
                        set download_url [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
                        set package_url $download_url

                        # Getting and setting deadline
                        set assignment_deadline [db_string assignment_deadline "select end_date from im_freelance_assignments where assignment_id =:assignment_id" -default ""]
                        set assignment_deadline_formatted [lc_time_fmt $assignment_deadline "%q %X"]

                        set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_subject]"
                        set body "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_body]"

                        set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
                        if {$signature ne ""} {
                            append body [template::util::richtext::get_property html_value $signature]
                        }

                        intranet_chilkat::send_mail \
                            -to_party_ids $assignee_id \
                            -from_party_id $project_lead_id \
                            -subject $subject \
                            -body $body \
                            -no_callback

                        im_freelance_notify \
                        -object_id $assignment_id \
                        -recipient_ids $assignee_id   
                    }
                }
                set success 1
                set action_done "accepted"
                set modal_title "[lang::message::lookup $locale sencha-portal.assignment_created_title]"
                set message "[lang::message::lookup $locale sencha-portal.assignment_created_message]"
            }
        }
        "deny" {
            set existing_assignee_id [db_string assigned_p "select assignee_id from im_freelance_assignments where freelance_package_id = :freelance_package_id
                and assignment_type_id = :assignment_type_id
                and assignment_status_id in (4222,4224,4225,4226) limit 1" -default 0]
            set task_type [string tolower [im_category_from_id -translate_p 0 $assignment_type_id]]
            set task_id [db_list tasks_from_assignment "select min(trans_task_id) from im_freelance_packages_trans_tasks fptt, im_freelance_assignments fa where fa.freelance_package_id = fptt.freelance_package_id and fa.assignment_id = :assignment_id and assignment_type_id = :assignment_type_id"]
            set target_language_id [db_string target "select target_language_id from im_trans_tasks where task_id = :task_id"]
            set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]  
            if {$existing_assignee_id eq $assignee_id} {
                im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id \
                -message "DENY AFTER ACCEPT - <a href='$assignments_url'>$project_nr</a> :: $task_type tried to be denied by [im_name_from_id $assignee_id] for [im_category_from_id $target_language_id]" \
                -severity 1
            } else {
                db_dml update "update im_freelance_assignments set assignment_status_id = 4223 where assignment_id = :assignment_id"
                
                im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id \
                -message "<a href='$assignments_url'>$project_nr</a> :: $task_type denied by [im_name_from_id $assignee_id] for [im_category_from_id $target_language_id]" \
                -severity 1
            }
            set success 1
            set action_done "denied"
            set modal_title "[lang::message::lookup $locale sencha-portal.assignment_denied_title]"
            set message "[lang::message::lookup $locale sencha-portal.assignment_denied_message]"
        }
        "download_package" {
            # Download package

            # Record that the package has been downloaded and the assignment has been started
            # (if the assignee is the person doing the downloading)
            if {[db_0or1row assignment_info "select assignee_id, assignment_status_id, assignment_id, freelance_package_id from im_freelance_assignments where assignment_id = :assignment_id and assignment_status_id in (4222, 4224,4225,4226,4227,4229)"]} {
                set read_p 0

                if {[im_user_is_employee_p $rest_user_id] || [acs_user::site_wide_admin_p -user_id $rest_user_id]} {
                    set read_p 1
                }

                if {!$read_p} {
                    if {[info exists assignee_id] && $rest_user_id eq $assignee_id} {
                        # Check if the assignee tries to download
                        set read_p 1
                    }
                }


                set file_path ""
	       
                if {$freelance_package_file_id ne "" && $freelance_package_file_id ne "undefined"} {

                    # set the package_file_id based on the assignment and user_id (to see if it is assignee or pm)
                    set file_path [db_string file "select file_path from im_freelance_package_files where freelance_package_file_id = :freelance_package_file_id" -default ""]
                }

                if {$read_p && [file readable $file_path]} {
                    switch $assignment_status_id {
                        4222 - 4229 {
                            # Update to work started
                            db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4224 where assignment_id = :assignment_id"           
                        }
                    }
                
                    set relative_file_path [string trimleft $file_path [im_filestorage_project_path $project_id]]
                    set package_download_url "[ad_url]/sencha-portal/download/project/$project_id/$relative_file_path"
		    
                    set success 1
                    set action_done "downloaded_package"
                    set modal_title "[lang::message::lookup $locale sencha-portal.fl_downloaded_package_title]"
                    set message "[lang::message::lookup $locale sencha-portal.fl_downloaded_package_message]"
                }
            } else {
                set success 0
                set action_done "downloaded_package"
                set modal_title "FAILED"
                set message "Could not download package"
            }
        }
        "package_request" {
            # Remind PM to upload package
            db_1row package_file_info "select project_lead_id, fp.project_id, project_nr from im_freelance_packages fp, im_projects p where fp.project_id = p.project_id and fp.freelance_package_id = :freelance_package_id"

            set assignee [person::name -person_id $assignee_id]
            set assignments_url [export_vars -base "/sencha-freelance-translation/assignments" -url {project_id}]
            set upload_url [export_vars -base "/sencha-freelance-translation/package-upload" -url {freelance_package_id {return_file_p 0} {return_url $assignments_url}}] 
            im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id -severity 0 -message "Package requested from ${assignee}. Please <a href='$upload_url'>upload the package</a> for $project_nr so they can start working"

            set success 1
            set action_done "reminded_pm"
            set modal_title "[lang::message::lookup $locale sencha-portal.pm_reminded_title]"
            set message "[lang::message::lookup $locale sencha-portal.pm_reminded_message]"
    
        }
        "review_delivery" {
            # Review delivery
            db_0or1row assignment_info "select assignee_id, assignment_status_id, assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id in (4222, 4224,4225,4226,4227,4229)"
            
            # Get the latest file delivered by the freelancer
            set delivery_file_id [db_string package "select max(freelance_package_file_id) as freelance_package_file_id
                from im_freelance_package_files fpf, acs_objects o
                where fpf.freelance_package_file_id = o.object_id
                and fpf.freelance_package_id = :freelance_package_id
                and o.creation_user = :assignee_id" -default ""]

            set file_path [db_string file_path "select file_path from im_freelance_package_files where freelance_package_file_id = :freelance_package_file_id" -default ""]

            if {$file_path ne ""} {
                set relative_file_path [string trimleft $file_path [im_filestorage_project_path $project_id]]
                set package_download_url "[ad_url]/sencha-portal/download/project/$project_id/$relative_file_path"

                set success 1
                set action_done "downloaded_delivery_files"
                set modal_title "[lang::message::lookup $locale sencha-portal.fl_downloaded_package_title]"
                set message "[lang::message::lookup $locale sencha-portal.fl_downloaded_package_message]"
            }
        }
        "delete_assignment" {
            # Check for purchase order
            if {$purchase_order_id eq ""} {

                # Check for admin
                if {$project_lead_id eq $rest_user_id || [acs_user::site_wide_admin_p -user_id $rest_user_id]} {
                    db_1row assignment_info "select im_name_from_id(assignment_type_id) as task_type, freelance_package_id from im_freelance_assignments where assignment_id = :assignment_id"
                    
                    # Remove from the tasks
                    set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks
                        where freelance_package_id = :freelance_package_id"]

                    foreach task_id $task_ids {
                        db_dml update_task "update im_trans_tasks set ${task_type}_id = null where task_id = :task_id"
                    }

                    db_dml update "update im_freelance_assignments set assignment_status_id = 4230 where assignment_id = :assignment_id"
                    set success 1
                    set action_done "delete_assignment"
                    set modal_title "[lang::message::lookup $locale sencha-portal.assignment_deleted]"
                    set message "[lang::message::lookup $locale sencha-portal.lt_assignment_deleted]"
                } else {
                    set success 0
                    set action_done "none"
                    set message "You can't delete an assignment for which you are not the project lead"
                    set modal_title "Permission denied"      
                }
            } else {
                set success 0
                set action_done "none"
                set message "You can't delete an assignment for which a purchase order exists"
                set modal_title "Purchase order exists"
            }
        }
    }
    set result "{\"success\": $success, \"action_done\":\"$action_done\",\"message\":\"$message\",\"modal_title\":\"$modal_title\",\"package_download_url\":\"$package_download_url\"}"
    im_rest_doc_return 200 "application/json" $result
    return
}

ad_proc -public intranet_rest::get::project_languages {
    -project_id
    -language_type  
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Get the list of languages in a project, based on language_type
    
    Returns information about the project languages including the sorting of the languages
    
    @param project_id ID of the project for which to query the languages
    @param language_type Type of the language (source / target)
    
    @return data Array of langauges
    @return_data id same as category_id
    @return_data category_id CategoryID of the language
    @return_data category_translated The translated name of the language
    @return_data object_name same as category_translated
    @return_data minimum_language_experience_level the minimum experience level required
    @return_data tree_sortkey sortkey of the category
    @return_data is_project_tl_p boolean value which tells us if langauge is 'core' target language of project
} {

    # Get locate for translation
    set locale [lang::user::locale -user_id $rest_user_id]
    
    # Set the object list
    set data [list]
    

    db_0or1row lang_experience_level "select im_categories.aux_int1 as minimum_language_experience_value
        from im_object_freelance_skill_map 
        left outer join im_categories on (im_categories.category_id = im_object_freelance_skill_map.skill_id)
        where object_id = :project_id and skill_type_id = 2028"

    if {[exists_and_not_null minimum_language_experience_value]} {
       set minimum_language_experience_level $minimum_language_experience_value
    } else {
        set minimum_language_experience_level 0
    }

    switch $language_type {
        source {
            set is_project_tl_p 0
            # Project's Source & Target Languages
            db_0or1row source_langs "select source_language_id, ch.parent_id
                        from    im_projects left outer join im_category_hierarchy ch on (child_id = source_language_id)
                        where   project_id = :project_id"
            
            set source_languages [list]
            if {[exists_and_not_null parent_id]} {
                set source_language_ids [im_sub_categories $parent_id]
            } else {
                set source_language_ids [im_sub_categories $source_language_id]
            }
            
            # Append all source langauges (and their children)
            foreach source_lang_id $source_language_ids {
                set tree_sortkey [db_string sort_key "select sort_order from im_categories where category_id = :source_lang_id" -default "$source_lang_id"]
                set category_id $source_lang_id
                set id $category_id
                set category_translated [im_category_from_id -locale $locale $source_lang_id]
                set object_name $category_translated
                lappend data [cog_rest::json_object]
            }
        }
        target {
            set target_language_ids [list]
            db_foreach target_langs "
                select  language_id as target_language_id, parent_id
                from    im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
                where   project_id = :project_id
            " {
                if {[exists_and_not_null parent_id]} {
                    set target_ids [im_sub_categories $parent_id]
                } else {
                    set target_ids [im_sub_categories $target_language_id]
                }
                foreach target_id $target_ids {
                    if {[lsearch $target_language_ids $target_id]<0} {
                        lappend target_language_ids $target_id
                    }
                }
            }
            
            foreach target_lang_id $target_language_ids {
                set tree_sortkey [db_string sort_key "select sort_order from im_categories where category_id = :target_lang_id" -default "$target_lang_id"]
                set category_id $target_lang_id
                set id $category_id
                set category_translated [im_category_from_id -locale $locale $target_lang_id]
                set object_name $category_translated
                set is_project_tl_p [db_string is_project_tl_sql "select 1 from im_target_languages where project_id =:project_id and language_id =:target_lang_id" -default 0]
                lappend data [cog_rest::json_object]

            }
        }
    }
    return [cog_rest::return_array]
}



ad_proc -public intranet_rest::get::sencha_project_files {
     -project_id
    { -requested_path "" }
    { -rest_user_id 0 }
} {
    Handler for GET rest calls to get all files (with folders) structure for given project_id

    It checks for the folders in the project_path
    If you have permission on the folder, you also have permission on all files and folders below that project_path (inheritance)

    Then it returns the list of files / folders

    @param project_id integer project for which to load the files
    @param requested_path string Path which we want to load files for relative to project_id

 
    @return project_files
    
    @return_project_files file_name string name of the file (including file extension)
    @return_project_files extension string extension of the file to use for  favicon. No extension = folder. If file but not extension, txt is assumed.
    @return_project_files file_size integer size of the file
    @return_project_files modification_date string time the file was last modified in ANSI format
    @return_project_files parent_folder string path of the folder relative to project_path
    @return_project_files download_url string url of file which is later use to download it. We should remove it later as we are gonna have different endpoint for that


} {
    
    set project_path [im_filestorage_project_path $project_id]

    if {$requested_path eq ""} {
        set find_path $project_path
    } else {
        set find_path ${project_path}/$requested_path
    }

    set find_cmd [im_filestorage_find_cmd]
    set project_files [list]

    # Permission checks
    # Return the list of allowed folders (parent_folders)
    # set allowed folders ......
    set allowed_folders [sencha_folder_permissions -project_id $project_id -user_id $rest_user_id]


    # Get the list of files in the path
    set folder_files [ad_find_all_files -max_depth 3 -include_dirs 1 $find_path]
    
    # Remove requested_path from list
    set idx [lsearch $folder_files $find_path]
    set folder_files [lreplace $folder_files $idx $idx]

    foreach file $folder_files {
        if {[file isfile $file]} {
            set dir_name [file dirname $file]
        } else {
            set dir_name $file
        } 
        # Check permission based on folder
        if {[lsearch $allowed_folders $dir_name]>-1} {  

            # Remove the project path
            set parent_folder [string trimleft $dir_name $project_path]
            set file_name [file tail $file]

            if {[file isdirectory $file]} {
                # strip out the filename from the parent folder
                # as this is a directory
                set parent_folder ""
                set parent_folder [string trimright $parent_folder $file_name]
                set download_url ""
                set extension ""
                set file_size ""
                set modification_date ""
            } else {
                set extension [file extension $file]
                set file_size [file size $file]
                set modification_date [ns_fmttime [file mtime $file] "%Y-%m-%d %H:%M"]
                
                set download_url "[ad_url]/sencha-portal/download/project/$project_id/$parent_folder/$file_name"
            }
            lappend project_files [cog_rest::json_object]      
        }
    }

    return [cog_rest::return_array]

}

ad_proc -public intranet_rest::get::sencha_move_uploaded_file {
    -file:required
    -filename:required
    -project_id:required
    { -overwrite_file_p 0}
    { -destinated_path ""}
    { -rest_user_id 0 }
} {
    Handler for GET rest calls to move just uploaded file from /tmp/ directory to desired path

    @param file string name of file in /tmp folder
    @param destinated_path string folder of destination relative to project_path
    @param filename string finale filename to be saved in desired destination folder
    @param project_id object im_project::read project_id, might come in handy in case we send empty desired_path
    @param overwrite_file_p boolean If set to 1, overwrite existing file. Otherwise create new version. Defaults to 0 (new version)

    @return success Can be true or false, depending on whether the file upload was successful
    @return modal_title Title of the modal to display. Depends on the result of the upload
    @return message Message of the modal window. No message appears if there were not upload issues.
    @return action_type Could be "info_modal" or "error_modal" if failed.
    @return new_filename New filename given (especially if we created a new version)
} {

    set file_path_in_tmp $file

    set project_path [im_filestorage_project_path $project_id]
    if {$destinated_path eq ""} {
        set destinated_path $project_path
    } else {
        set destinated_path ${project_path}$destinated_path
    }

    set allowed_folders [sencha_folder_permissions -project_id $project_id -user_id $rest_user_id -type "write"]

    if {[lsearch $allowed_folders $destinated_path]>-1} {
        # User is allowed to write into this folder (probably)

        # Check if file exists
        set new_file_full_path "$destinated_path/$filename"

        if {![file exists $new_file_full_path] || $overwrite_file_p} {
            file rename -force $file $new_file_full_path
            set new_filename "[file rootname [file tail $new_file_full_path]][file extension $new_file_full_path]"
            
            set success true
            set modal_title ""
            set message ""
            set action_type ""
        } else {

            # Extracting filename and extension
            set desired_filename [file rootname [file tail $filename]]
            set desired_extension [file extension $filename]

            # We must figure out new name for file
            set folder_files [ad_find_all_files -include_dirs 0 $destinated_path]
            set folder_files_without_paths [list]
            foreach folder_file $folder_files {
                set folder_file_filename [file rootname [file tail $folder_file]]
                set folder_file_extension [file extension $folder_file]
                lappend folder_files_without_paths "$folder_file_filename$folder_file_extension"
            }

            # Increase the version_number is the file already exists
            set version_number 2
            
            while {[lsearch $folder_files_without_paths "${desired_filename}_v${version_number}${desired_extension}"]>-1} {
                incr version_number
            }

            set new_filename "${desired_filename}_v${version_number}${desired_extension}"
            set new_file_full_path "$destinated_path/$new_filename"
            file rename -force $file_path_in_tmp $new_file_full_path

            set success false
            set action_type "info_modal"
            set modal_title [lang::message::lookup "" sencha-portal.problem_with_your_upload_file_exists_modal]
            set message [lang::message::lookup "" sencha-portal.problem_with_your_upload_file_exists_appended_version]
        }

        # Notify the  PM Of the upload
        if {[im_user_is_customer_p $rest_user_id]} {
            if {[db_0or1row project_lead "select project_lead_id, im_name_from_id(company_id) as customer_name, project_nr from im_projects where project_id = :project_id"]} {
                set portal_url [parameter::get_from_package_key -package_key "sencha-portal" -parameter "ProjectManagersPortalUrl"]
                set project_url "[export_vars -base $portal_url -url {project_id}]"
                im_freelance_notify -severity 1 -recipient_ids $project_lead_id -object_id $project_id -message [lang::message::lookup "" sencha-portal.pm_notify_new_file]
            }
        }
    } else {
        set success false
        set modal_title [lang::message::lookup "" sencha-portal.problem_with_your_upload_modal_title]
        set message [lang::message::lookup "" sencha-portal.problem_with_your_upload_permission]
        set new_filename ""
        set action_type "error_modal"

        # Delete the tmp_file
        file delete -force $file_path_in_tmp
    }


    return "[cog_rest::json_object]"
}


ad_proc -public intranet_rest::post::sencha_portal_create_project {
    -company_id:required
    -project_type_id:required
    -source_language_id:required
    -target_language_ids:required
    { -project_lead_id ""}
    { -subject_area_id ""}
    { -final_company_id ""}
    { -customer_contact_id ""}
    { -company_project_nr ""}
    { -original_files ""}
    { -reference_files ""}
    { -projectTasks ""}
    { -start_date ""}
    { -deadline ""}
    { -language_experience_level_id ""}
    -project_name:required
    { -project_nr ""}
    { -company_office_id ""}
    { -project_source_id ""}
    { -comment ""}
    { -do_analysis_p 0 }
} {
    Handler for POST calls on the project. Will use any variable passed through the query_hash_pairs to create / update the project
    if the variable has been defined as attributes for the object_type.

    For variables starting with "skill_", these will be added as freelancer skills to the project. So are source_language_id, target_language_ids, subject_area_id and
    language_experience_level_id (the level for the skills in languages).

    @param company_id integer Company in which to create the project
    @param project_type_id integer Project Type of the project to create
    @param source_language_id category "Intranet Target Languages" Source Language for the project. Typically the language the source material is in
    @param target_language_ids category_array "Intranet Target Languages" Target Languages into which to translate the source material
    @param project_lead_id id integer of user who is project leader
    @param subject_area_id integer Subject Area of the project (determines pricing among other things)
    @param final_company_id integer Final company for this translation (determines the reference material and Termbase / Translation Memory to use)
    @param company_project_nr string Project nr used only by customer company
    @param customer_contact_id integer Contact in the company who requested this project
    @param original_files json_array Path of files which were uploaded previously for this project
    @param reference_files json_array Path of reference files which were uploaded previously for this project
    @param projectTasks object_array im_trans_tasks::read Array of tasks
    @param start_date string potential start date of project
    @param deadline string potential end_date of project
    @param language_experience_level_id category "Intranet Language Experience" Required experience level for this project. Used (previously) to differentiate certified translations
    @param project_name string Name of the project
    @param project_nr string Number of the project. Mostly automatically determined now though
    @param company_office_id integer id of company office used with that project
    @param project_source_id integer id of project source type (category id)
    @param comment string Comment the customer provided
    @oaram do_analysis_p boolean Switch which decided if we should execute MemoQ file analysis. 1 = Yes, 0 = No.


    @return project_id integer ID of the created project

} {

    callback im_project_before_create -object_id 0 -status_id [im_project_status_potential]
   
    # Extract a key-value list of variables from JSON POST request
    set random_errors_token "[ad_generate_random_string]"

    set create_quote_p 0
  
    #-------------------------------------------------------
    # Check if we are creating new project or editing existing
    set project_exists_p 0
    if {[exists_and_not_null project_id]} {
        if {[ad_var_type_check_number_p $project_id]} {
            set project_exists_p [db_string project_exists "
                        select count(*)
                        from im_projects
                        where project_id = :project_id
                "]
        } else {
            set project_name $project_id
        }
    }

    
    if {$project_exists_p eq 1} {
        # Updating the project

        set sql "update im_projects set"
    
        if {[exists_and_not_null final_company_id]} {
            append sql " final_company_id=:final_company_id,\n"
        }
        if {[exists_and_not_null company_id]} {
            append sql " company_id=:company_id,\n"
        }
        if {[exists_and_not_null project_type_id]} {
            append sql " project_type_id=:project_type_id,\n"
        }
        if {[exists_and_not_null project_nr]} {
            append sql " project_nr=:project_nr,\n"
        }
        if {[exists_and_not_null project_name]} {
            append sql " project_name=:project_name,\n"
        }
        if {[exists_and_not_null customer_contact_id]} {
            append sql " company_contact_id=:customer_contact_id,\n"
        }
        if {[exists_and_not_null expected_quality_id]} {
            append sql "expected_quality_id=:expected_quality_id,\n"
        }
        if {[exists_and_not_null subject_area_id]} {
            append sql " subject_area_id=:subject_area_id,\n"
        }
        if {[exists_and_not_null source_language_id]} {
        append sql " source_language_id=:source_language_id,\n"
        }
    
        append sql " project_id=:project_id where project_id=:project_id"
    
        set target_language_ids [split $target_language_ids ","]
        db_transaction {
            db_dml update_im_projects $sql
            db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
        }
    
        db_transaction {
            foreach target_language_id $target_language_ids {
                db_dml insert_im_target_language "insert into im_target_languages values ($project_id, $target_language_id)"
            }
        }
    
        if {[apm_package_installed_p "intranet-freelance"]} {
        
            db_dml delete_exising_skills "delete from im_object_freelance_skill_map where object_id =:project_id"
            
            #later we need to have possibiliy to return 2028 id by something like [im_freelance_skill_type_subject_area]
            if {$language_experience_level_id ne ""} {
                im_freelance_add_required_skills -object_id $project_id -skill_type_id 2028 -skill_ids $language_experience_level_id
            }
                
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
            #im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_expected_quality] -skill_ids $expected_quality_id
            
            foreach target_language_id $target_language_ids {
                im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $target_language_id
            }
        }

    } else {
        
       # If we do not have project_lead_id, we use rest_user_id
       if {$project_lead_id eq ""} {
           set project_lead_id $rest_user_id
       }
       sencha_errors::display_errors_and_warnings -object_id $random_errors_token

       # Project source id
       if {$project_source_id eq ""} {
           if {$app_name eq "CustomersPortal"} {
               set project_source_id [im_project_source_customer_portal]
           } else {
               set project_source_id [im_project_source_pm_app]
           }
       }
  
       set project_id [im_translation_create_project \
               -company_id $company_id \
               -project_name $project_name \
               -project_nr $project_nr \
               -project_type_id $project_type_id \
               -project_lead_id $project_lead_id \
               -source_language_id $source_language_id \
               -target_language_ids $target_language_ids \
               -subject_area_id $subject_area_id \
               -final_company_id $final_company_id \
               -project_source_id $project_source_id \
               -no_callback]
       
       if {$project_id eq 0} {
       # There was  an error creating the project, try to find out if it is because it was a duplicate
       set project_id [db_string project_id "select project_id from im_projects where
            (   upper(trim(project_name)) = upper(trim(:project_name)) OR
                upper(trim(project_nr)) = upper(trim(:project_nr)) OR
                upper(trim(project_path)) = upper(trim(:project_name))
            )" -default 0]
       
       ns_log Notice "Tried to find the project ... $project_id .. $project_nr"
       if {$project_id eq 0} {
           set project_id $random_errors_token
           set error_msg "Failed to create project. We sadly don't know why"
           sencha_errors::add_error -object_id $random_error_token -problem $error_msg
           set redirect_url "/intranet"
       } else {
           set error_msg "Found project already existing"
           sencha_errors::add_warning -object_id $project_id -problem $error_msg
       }
       
       } else {

       # Update company_project_nr 
       db_dml update_company_project_nr "update im_projects set company_project_nr =:company_project_nr where project_id = :project_id"
       
       # Set project status to potential
       db_dml update_project_status "update im_projects set project_status_id = [im_project_status_inquiring], company_contact_id = :customer_contact_id where project_id = :project_id"
       
       # Update company_office_id 
       if {$company_office_id ne ""} {
           db_dml update_company_project_nr "update im_projects set company_office_id =:company_office_id where project_id = :project_id"
       }
       
       # Update project with values found in the hash array
       im_rest_object_type_update_sql \
           -rest_otype im_project \
           -rest_oid $project_id \
           -hash_array $query_hash_pairs
       
       # ---------------------------------------------------------------
       # Make sure we have the skills
       # ---------------------------------------------------------------
       if {[apm_package_installed_p "intranet-freelance"]} {
           
           # ---------------------------------------------------------------
           # We need to store source and target language again
           # ---------------------------------------------------------------
           
           #later we need to have possibiliy to return 2028 id by something like [im_freelance_skill_type_subject_area]
           if {$language_experience_level_id ne ""} {
           im_freelance_add_required_skills -object_id $project_id -skill_type_id 2028 -skill_ids $language_experience_level_id
           }
           
           im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
           
           foreach key [array names hash_array] {
           if {[string match "skill_*" $key]} {
               set skill_type_id [lindex [split $key "_"] 1]
               im_freelance_add_required_skills -object_id $project_id -skill_type_id $skill_type_id -skill_ids  $hash_array($key)
           }
           }
       }
       
        # We need to catch the audit, as the project might still be created
       
        catch {
            cog::callback::invoke -object_type "im_project" -object_id $project_id  -type_id $project_type_id -status_id [im_project_status_potential] -action after_create
        }
       
        # ---------------------------------------------------------------
        # Deal with the uploaded files array
        # ---------------------------------------------------------------
        if {[exists_and_not_null original_files]} {
            set uploaded_files_original $original_files
            set source_folder [im_trans_task_folder -project_id $project_id -folder_type source]
            set project_dir [im_filestorage_project_path $project_id]
            set source_dir "${project_dir}/$source_folder"
            foreach original_files [lindex $original_files 1] {
                array set one_file [lindex $original_files 1]
                file rename -force $one_file(path) "${source_dir}/$one_file(filename)"
            }
           
            catch {callback im_project_after_file_upload -project_id $project_id -type_id $project_type_id -status_id [im_project_status_potential]}
           
            # Check if we have auto created tasks without analyis
            set auto_created_tasks_p 0
            foreach file [lindex $uploaded_files_original 1] {
                array set one_file [lindex $file 1]
                set task_filename $one_file(filename)
                set task_uom_id [im_uom_s_word] 
                set count_created_tasks [db_string count_created_tasks "select count(*) from im_trans_tasks where project_id = :project_id and task_filename =:task_filename " -default 0]
                if {$count_created_tasks eq 0} {
                    ns_log Debug "DEBUGING inserted $task_filename for project: $project_id"
                    im_task_insert $project_id $task_filename $task_filename 0 $task_uom_id $project_type_id $target_language_ids
                    set auto_created_tasks_p 1
                }
            }

            if {!$auto_created_tasks_p} {
                # Only create the quote if we have not auto created tasks with a unit of 0
                set create_quote_p [db_string tasks_exist "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]
            }
        }

       # Reference files
       if {[exists_and_not_null reference_files]} {
           set ref_files_folder_name [parameter::get_from_package_key -package_key "sencha-portal" -parameter "ReferenceFileFolder" -default "Projektinfos"]
           set project_dir [im_filestorage_project_path $project_id]
           set source_dir "${project_dir}/$ref_files_folder_name"
           foreach reference_files [lindex $reference_files 1] {
               array set one_file [lindex $reference_files 1]
               file rename -force $one_file(path) "${source_dir}/$one_file(filename)"
           }
       }

       
        # ---------------------------------------------------------------
        # Create tasks if necessary
        # ---------------------------------------------------------------
        if {[exists_and_not_null projectTasks]} {
            foreach projectTask [lindex $projectTasks 1] {
                array unset one_task
                array set one_task [lindex $projectTask 1]
           
                if {[exists_and_not_null one_task(units_of_measure)]} {
                    set task_uom_id $one_task(units_of_measure)
                } else {
                    set task_uom_id 324 ;# Source words
                }
                set task_status_id 340
           
                if {[exists_and_not_null one_task(name)]} {
                    # Insert into the project
                    set ip_address [ad_conn peeraddr]
                    if {[exists_and_not_null one_task(description)]} {
                        set description $one_task(description)
                    } else {
                        set description ""
                    }
               
                    if {[exists_and_not_null one_task(units)]} {
                        set billable_units $one_task(units)
                    } else {
                        set billable_units ""
                    }
                    set task_name $one_task(name)
               
                    foreach target_language_id $target_language_ids {
               
                        set new_task_id [im_exec_dml new_task "im_trans_task__new (
                                    null,                   -- task_id
                                    'im_trans_task',        -- object_type
                                    now(),                  -- creation_date
                                    :rest_user_id,               -- creation_user
                                    :ip_address,            -- creation_ip
                                    null,                   -- context_id
                        
                                    :project_id,            -- project_id
                                    :project_type_id,          -- task_type_id
                                    :task_status_id,        -- task_status_id
                                    :source_language_id,    -- source_language_id
                                    :target_language_id,    -- target_language_id
                                    :task_uom_id            -- task_uom_id
                            )"]
               
                        if {[catch {db_dml update_task "update im_trans_tasks set
                                description = :description,
                                task_name = :task_name,
                                task_units = :billable_units,
                                billable_units = :billable_units
                            WHERE
                            task_id = :new_task_id"}]} {
                        
                        # Most likely the task name was provided twice, create with copy
                        set task_name "$task_name (copy)"
                        db_dml update_task "update im_trans_tasks set
                                description = :description,
                                task_name = :task_name,
                                task_units = :billable_units,
                                billable_units = :billable_units
                            WHERE
                            task_id = :new_task_id"
                        }
                        if {!$auto_created_tasks_p} {
                            # Even if we have tasks with units we should not create the quote
                            # if we have auto_created_tasks as this will not be correct
                            set create_quote_p 1
                        }
                    }
                }
            }
        }
       

       #---------------------------------------------------------------
       # Add comment as note
       #---------------------------------------------------------------
       if {$comment ne ""} {
           set note [string trim $comment]

           set note [template::util::richtext::create $note "text/html" ]
           set note_type_id [im_note_type_project_info]

           set note_id [db_exec_plsql create_note "
                        SELECT im_note__new(
                                NULL,
                                'im_note',
                                now(),
                                :rest_user_id,
                                '[ad_conn peeraddr]',
                                null,
                                :note,
                                :project_id,
                                :note_type_id,
                                [im_note_status_active]
                        )
                "]
       }

       # Notify PM if we call this procedure from PM Portal APP
       if {$app_name eq "CustomersPortal"} {

           set project_nr [db_string get_project_nr "select project_nr from im_projects where project_id =:project_id" -default ""]
           set project_link "[ad_url]/sencha-assignment/project-manager-app?project_id=$project_id"
           set default_message "Quote missing for inquired project <a href='$project_link'>$project_nr</a>"
           set message [lang::message::lookup "" sencha-freelance-translation.lt_quote_missing_for_inquired_project $default_message]
    
           im_freelance_notify -object_id $project_id -recipient_ids $project_lead_id -severity 0 -message $message
                
           set subject "Quote missing for inquired project $project_nr"
           set body $message

       }

       set redirect_url [im_biz_object_url $project_id]
       if {$create_quote_p} {
           set quote_id [im_trans_invoice_create_from_tasks -project_id $project_id -cost_type_id [im_cost_type_quote]]
           set redirect_url [im_biz_object_url $quote_id]
           set amount_sql "select sum(price_per_unit * item_units) from im_invoice_items where invoice_id =:quote_id"
           set quote_amount [db_string invoice_amount $amount_sql]
           if {$quote_amount > 0} {
           set return_url [im_biz_object_url $project_id]
           set redirect_url "[ad_url][apm_package_url_from_key "intranet-invoices"]view?invoice_id=$quote_id&return_url=$return_url"
           }
       }
       
       if {$start_date eq ""} {
          set start_date [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
       } 

       db_dml update_project "update im_projects set start_date =:start_date where project_id=:project_id"
       if {$deadline eq ""} {
           # Calculate the end date
            set start_date [db_string start_date "select to_char(start_date, 'YYYY-MM-DD HH24:MI') from im_projects where project_id=:project_id"]
           im_translation_update_project_dates -project_id $project_id -start_timestamp $start_date
       } else {
           db_dml update_project "update im_projects set end_date =:deadline where project_id=:project_id"
       }

       }
   }
    
    set hash_array(rest_oid) "$project_id"
    #set hash_arry(project_id) "$project_id"
    
    if {![info exists redirect_url]} {
        set redirect_url [im_biz_object_url $project_id]
    }
    set hash_array(redirect_url) $redirect_url

    # Get i18n success message
    set locale [lang::user::locale -user_id $rest_user_id]
    set message [lang::message::lookup $locale sencha-portal.project_created_success_message]
    set modal_title [lang::message::lookup $locale sencha-portal.project_created_success_modal_title]


    return "{\"success\": 1, \"message\":\"$message\", \"modal_title\":\"$modal_title\", \"project_id\":$project_id}"
}


ad_proc -public intranet_rest::get::sencha_update_project_company_contact {
    -project_id
    -company_contact_id
    { -rest_user_id 0 }
} {
    Handler for GET rest calls to change current company contact to new one.
    Used in CustomersPortal.

    In future we might want to have some kind of more generic procedure to save/edit proejct data

    @param project_id integer project for which we need new cusomter contact
    @param company_contact_id integer user_id of new company contact used within that project

    @return data Object with result of action
    @return_data success integer (boolean) result of action true/false
    @return_data modal_title string title (success message) to be displayed as modal title
    @return_data message string (success message) to be displayed in modal in portal
} {

    set success true
    set view_p 0

    # We need user language
    set locale [lang::user::locale -user_id $rest_user_id]

    # Check if the customer is a member of the company of the project
    if {[im_user_is_customer_p $rest_user_id]} {
        set company_id [db_string company "select company_id from im_projects where project_id = :project_id"]
        if {[im_biz_object_member_p $rest_user_id $company_id]} {
            set view_p 1
        }
    }    
    
    # Execute update query
    db_dml update_project "update im_projects set company_contact_id =:company_contact_id where project_id=:project_id"

     
    set message [lang::message::lookup $locale sencha-portal.save_contact_result_modal]
    set modal_title [lang::message::lookup $locale sencha-portal.save_contact_modal_title]

    return [cog_rest::json_object]
}



ad_proc -public im_rest_get_custom_project_take_over {
    -project_id
    { -new_project_lead_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest callls which allows to take over project.
    It literally means updating im_projects project_lead_id.
    If no new_project_lead_id is provided, we use rest_user_id as default.

    @param project_id id of project in which we want to do the 'take_over'
    @param new_project_lead_id id of user who now will became new pm (new project_lead_id in `im_projects`)

    @return success_p result of project take over. Boolean.
    @return message success/error message. Usually to be displayed in modal window
    @return modal_title title to be displayed as success/error modal
} {
    
    # We need user language
    set locale [lang::user::locale -user_id $rest_user_id]

    set success_p 0
    set modal_title [lang::message::lookup $locale sencha-portal.insufficient privileges_modal_title]
    set message [lang::message::lookup $locale sencha-portal.insufficient privileges_message]

    if {$new_project_lead_id eq ""} {
        set new_project_lead_id $rest_user_id
    }

    # Permissions check 
    im_project_permissions $rest_user_id $project_id view_p read_p write_p admin_p
    if {$admin_p} {
        # Update project_lead_id
        db_dml update_project_lead_id "update im_projects set project_lead_id =:new_project_lead_id where project_id=:project_id"
        # Add user as project member, only in case he is still not a member
        set member_role_id [im_biz_object_role_full_member]
        set project_member_ids [im_biz_object_member_ids $project_id]
        if {[lsearch $project_member_ids $new_project_lead_id]<0} {
            callback im_before_member_add -user_id $new_project_lead_id -object_id $project_id
            im_biz_object_add_role $new_project_lead_id $project_id $member_role_id
            cog::callback::invoke -object_id $project_id -action "after_update"
        } 
        # We need project_name and project_nr to display it in success modal message
        db_1row project_info "select project_nr, project_name from im_projects where project_id = :project_id"
        set success_p 1
        set modal_title [lang::message::lookup $locale sencha-portal.any_success_modal_title]
        set message [lang::message::lookup $locale sencha-portal.project_take_over_message]
    }

    # Generating JSON return values from @params of this proc
    set result [im_rest_json_object]
    
    im_rest_doc_return 200 "application/json" $result
    return

}

