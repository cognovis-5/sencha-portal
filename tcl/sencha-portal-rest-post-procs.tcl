# /packages/sencha-portal-rest-post-procs

ad_library {
	Rest POST Procedures for the sencha-portal package
	
	@author michaldrn@wp.pl
}



ad_proc -public im_rest_post_object_type_sencha_rfq {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    set success 1
    set error ""
    set new_rfq_id ""
    
    array set query_hash [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]
    
    # Setting 'required' variables
    set required_vars [list first_names last_name email cost_center_id quality_level_id project_type_id source_language_id target_languages_ids]
    foreach var $required_vars {
	    if {![exists_and_not_null query_hash($var)]} {
	        im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"
	        #sencha_errors::add_error -object_id $random_errors_token -field "$var"  -problem "this is required field"
	    } else {
	        set $var $query_hash($var)
	    }
    }
    # Setting 'optional' variables
    set optional_vars [list company_name telephone comment country_code deadline deadline_time creation_date customer_reference_number subject_area_id]
        foreach var $optional_vars {
	    if {![info exists query_hash($var)]} {
	        set $var ""
	    } else {
	        set $var $query_hash($var)
        }
    }
    
    set matched_user_id [db_string first_last_name_exists_p "
	   	select	user_id
	   	from	cc_users
	   	where	lower(trim(first_names)) = lower(trim(:first_names)) and
	   		lower(trim(last_name)) = lower(trim(:last_name)) and
	   		lower(trim(email)) = lower(trim(:email))
	   	" -default ""]
    
    set matched_company_id ""
    if {$company_name ne ""} {
	set matched_company_id [db_string company_exists_p "
	       	select	company_id
	   	    from	im_companies
	   	    where	lower(trim(company_name)) = lower(trim(:company_name))
	   	    " -default ""]
	
	regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path
	if {$matched_company_id eq ""} {
	    set matched_company_id [db_string company_exists "select company_id from im_companies where company_path = :company_path" -default ""]
	}
    } else {
        if {$matched_user_id ne ""} {
            db_0or1row matched_user_primary_company_sql "select company_id as matched_company_id from im_companies where primary_contact_id =:matched_user_id limit 1"
        }
    }

    if {$creation_date eq ""} {
        set systemTime [clock seconds]
        set current_date [clock format $systemTime -format "%Y-%m-%d %H:%M:%S"]
    } else {
        set current_date $creation_date
    }
    
    catch {
        set new_rfq_id [db_string insert_rfq "select external_rfq__new(:first_names, :last_name, :cost_center_id, :company_name, :email, :country_code, :telephone, :quality_level_id, :project_type_id, :source_language_id, :target_languages_ids, :matched_user_id, :matched_company_id, :comment, :current_date, :deadline, :customer_reference_number, :subject_area_id)" -default 0]
    } errmsg

    if {[string length $errmsg] > 0} {
    	set random_errors_token "[ad_generate_random_string]"
    	sencha_errors::add_error -object_id $random_errors_token -field "$var"  -problem "$errmsg"
    	sencha_errors::display_errors_and_warnings -object_id $random_errors_token -action_type "refresh" 

    }
    
    set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]
    set rfq_files_folder "$base_path_unix/external_rfq"
    
    # That code (files handling) works only with wordpress plugin
    # We need to modify it to work with e.g Customers Portal
    # Wordpress Plugin (written in PHP) sends files data differently than sencha upload grid
    set files_json_list $query_hash(uploaded_filename)
    set files_json_list [lindex $files_json_list 1]
    foreach uploaded_file $files_json_list {
        array set file_dataset [lindex [util::json::parse $uploaded_file] 1]
        set filename $file_dataset(path)
        set original_filename $file_dataset(original_filename)
        set extension $file_dataset(extension)
        set size [file size "$rfq_files_folder/$filename"]
        set res [db_string insert_rfq_file  "select external_rfq_file__new(:new_rfq_id, :filename , :original_filename, :extension, :size)" -default 0]
    }

    

    
    set result "{\"success\":$new_rfq_id , \"error\": \"$error\",\n\"data\": \[\n\n\]}"
    im_rest_doc_return 200 "application/json" $result
    
    set hash_array(rest_oid) "21342"
    set hash_array(result) $result

    return [array get hash_array]

}




ad_proc -public im_rest_post_object_type_sencha_portal_upload_package {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {

    @param freelance_package_id id of package
    @param uploaded_files Path of files which were uploaded previously for this project
    @param return_file_p boolean value which decides upload type (return_file_p = 1 means its from FreelancersPortal, otherwise PM App)

} {

	set success 1
	

	array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]
	set freelance_package_id $hash_array(freelance_package_id)
	set uploaded_files $hash_array(uploaded_files)

	set return_file_p 0

    if {[exists_and_not_null hash_array(return_file_p)]} {
	    set return_file_p $hash_array(return_file_p);
	}
    
    if {[exists_and_not_null hash_array(ratings)]} {
	    set ratings $hash_array(ratings)
    } else {
	set ratings ""
    }

    if {[exists_and_not_null hash_array(comment)]} {
	    set comment $hash_array(comment);
    } else {
	set comment ""
    }

	db_1row package_info "select p.project_id, p.project_nr, p.project_lead_id, freelance_package_name, package_type_id from im_freelance_packages fp, im_projects p
	 where freelance_package_id = :freelance_package_id
	 and fp.project_id = p.project_id"

	db_0or1row assignee_info "select assignee_id, assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id in (4222,4224,4225,4227,4229)"

	db_1row task_info "select im_name_from_id(source_language_id) as source_language,
	im_name_from_id(target_language_id) as target_language
	from im_trans_tasks where task_id = (
	select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id limit 1)"


    # Ratings
    set save_ratings_p 0
    set previous_assignment_id 0
	if {[im_assignment_is_rating_fl_possible -assignment_id $assignment_id] && $return_file_p} {
		 set previous_assignment_id [im_assignment_previous_stage_assignment_id -assignment_id $assignment_id]
	     array set parsed_json $ratings
	     set ratings_json_list $parsed_json(_array_)
	     set save_ratings_p 1

	}

    set location [util_current_location]
    if {$assignee_id ne ""} {
     	set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
		set locale [lang::user::locale -user_id $assignee_id]
	}

	# Use the freelancer company name in case needed for communication
	set internal_company_name [im_name_from_id [im_company_freelance]]

	set type [im_category_from_id -translate_p 0 $package_type_id]

    if {$return_file_p} {

		set upload_folder [im_trans_task_folder \
			-project_id $project_id \
			-target_language $target_language \
			-folder_type $type \
			-return_package]
		set package_file_type_id 601

    } else {
		set upload_folder [im_trans_task_folder \
			-project_id $project_id \
			-target_language $target_language \
			-folder_type $type]
		set package_file_type_id 600
    }
   
	if {[exists_and_not_null uploaded_files]} {
	   foreach uploaded_file [lindex $uploaded_files 1] {
		    array set one_file [lindex $uploaded_file 1]
		    set extension [file extension $one_file(filename)]
		    set freelance_package_file_name "${freelance_package_name}$extension"
		    set new_file_path "[im_filestorage_project_path $project_id]/${upload_folder}/$freelance_package_file_name"
		    file rename -force $one_file(filepath) $new_file_path

		    set package_exists_p [db_string exists_p "select 1 from im_freelance_package_files where freelance_package_id = :freelance_package_id and package_file_type_id = :package_file_type_id limit 1" -default 0]
		    if {!$package_exists_p} {
			    set ip_address [ad_conn peeraddr]
			    set freelance_package_file_id [db_string create_file "select im_freelance_package_file__new(:rest_user_id,:ip_address,:freelance_package_id,:freelance_package_file_name,:package_file_type_id,620,:new_file_path) from dual"]
		    }
	   }
	}

	
	
	if {$return_file_p} {

		db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4225 where assignment_id = :assignment_id"


		set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
	    set package_link [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
		# Notify the PM about the upload
		set assignee_name [im_name_from_id $assignee_id]
		im_freelance_notify \
			-object_id $assignment_id \
			-recipient_ids $project_lead_id \
		    -message "[_ sencha-freelance-translation.notify_return_package]"


		
		# Set the assignment status to work delivered (4225)
		db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4225 where assignment_id = :assignment_id"
		
		# Save ratings
		if {$save_ratings_p eq 1 && $previous_assignment_id > 0} {
			foreach single_rating $ratings_json_list {
		        array set rating_dataset [lindex $single_rating 1]
		        im_assignment_quality_report_new -creation_user_id $rest_user_id -assignment_id $previous_assignment_id -quality_type_id $rating_dataset(id_category) -quality_level_id $rating_dataset(value) -comment $comment
		    }
		} 

		# Littke bit hacky, but for the time beign will work
		if {$comment ne ""} {
			# Create a message for the PM
			set log_id [im_freelance_message_send \
				-sender_id $assignee_id \
				-recipient_ids $project_lead_id \
				-subject "$freelance_package_name Comment" \
				-body "$comment" \
				-context_id $assignment_id \
				-project_id $project_id]
		}

        if {$assignee_id ne ""} {

			set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_package_uploaded_message_subject]"
			set body "[lang::message::lookup $locale sencha-freelance-translation.lt_package_uploaded_message_body]"
		
			im_freelance_notify \
				-object_id $assignment_id \
				-recipient_ids $assignee_id \
				-message "$body"

			set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
			if {$signature ne ""} {
				append body [template::util::richtext::get_property html_value $signature]
			}
		
			intranet_chilkat::send_mail \
				-to_party_ids $assignee_id \
				-from_party_id $project_lead_id \
				-subject $subject \
				-body $body \
				-no_callback
		}
						
	} else {
		if {$assignee_id ne ""} {
			# Set the assignment status to work ready (4229)
			db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4229 where assignment_id = :assignment_id"
			
      	   set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
           set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]
           set download_url [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
           set package_url $download_url

           # Getting and setting deadline
           set assignment_deadline [db_string assignment_deadline "select end_date from im_freelance_assignments where assignment_id =:assignment_id" -default ""]
           set assignment_deadline_formatted [lc_time_fmt $assignment_deadline "%q %X"]
           
           set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_subject]"
           set body "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_body]"

            set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
            if {$signature ne ""} {
            		append body [template::util::richtext::get_property html_value $signature]
            }


			acs_mail_lite::send \
				-send_immediately \
				-to_addr [party::email -party_id $assignee_id] \
				-from_addr [party::email -party_id $project_lead_id] \
				-subject $subject \
				-body $body \
				-mime_type "text/html" \
				-no_callback \
			        -use_sender
				
			im_freelance_notify \
				-object_id $assignment_id \
				-recipient_ids $assignee_id
		}
	}

	set result "{\"success\":$success}"
	im_rest_doc_return 200 "application/json" $result

	set hash_array(rest_oid) $freelance_package_id
	set hash_array(result) $result

    return [array get hash_array]
}



