# /packages/sencha-portal-rest-procs

ad_library {
    Rest Procedures for the sencha-portal package
    @author michaldrn@wp.pl
}


ad_proc -public intranet_rest::get::sencha_key_account_info {
    { -app_name ""}
    -rest_user_id:required
} {
    Handler for GET rest calls to get account information (needed in Customer Portal App)

    @param app_name string name of the application for which to display the key_account_cell_phone

    @return data Data for the account

    # logged user data (in future we might want to move that to elswhere)
    @return_data logged_user_id integer id of currently logged user (we should probably get rid of that)
    @return_data logged_user_first_names string first_names of currently logged user (we probably should get rid of that)
    @return_data logged_user_last_name string last_name of currently logged user (we should probably get rid of that)
    @return_data logged_user_email string email of currently logged user (we should probably get rid of that)
    @return_data logged_user_company_id integer company_id of currently logged user
    @return_data logged_user_company_name string company name of currently logged user
    @return_data logged_user_locale string locale (language) of currently logged user

    # key account data
    @return_data key_account_id integer userid of the key account
    @example_data key_acount_id 625
    @return_data key_account_first_names string First name(s) of the key accounter
    @example_data key_account_first_names Mike
    @return_data key_account_last_name string Last name of the key accounter
    @return_data key_account_email string E-Mail of the key accounter
    @return_data key_account_work_phone string Work phone of the key accounter
} {
    set account_info_data ""

    set key_account_id ""
    set obj_ctr 1
    set komma ",\n"
    if {1 == $obj_ctr} { set komma "" }

    # First get currently logged user data 
    db_0or1row current_user "select user_id as logged_user_id, first_names as logged_user_first_names, last_name as logged_user_last_name, email as logged_user_email from cc_users where user_id =:rest_user_id"

    set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]
    set company_id [im_user_main_company_id -user_id $rest_user_id]
    
    # Hack to display some information if no main company for user is existing
    if {$company_id eq ""} {
        set company_id [im_company_internal]
    }

    db_0or1row company_info "select company_id, company_name from im_companies where company_id =:company_id"

    switch $app_name {
        "CustomersPortal" {
            set key_account_id [db_string get_key_account_id "select manager_id as key_account_id from im_companies where company_id =:company_id" -default ""]      
        }
        "ProjectManagersPortal" {
            set key_account_id [db_string get_key_account_id "select supervisor_id as key_account_id from im_employees where employee_id =:rest_user_id" -default ""]
        }
        "FreelancersPortal" {
            set freelancers_contact_email [parameter::get_from_package_key -package_key "sencha-portal" -parameter "GeneralFreelancerContact" -default ""]
            if {$freelancers_contact_email ne ""} {
                set key_account_id [db_string get_key_account_id "select user_id as key_account_id from cc_users where email =:freelancers_contact_email" -default ""]
            }
        }
    }
    
    # Hack to display some information if no key account is present.
    if {$key_account_id eq "" } { 
        set key_account_id $rest_user_id 
    }

    db_0or1row manager_data "select user_id as key_account_user_id, first_names as key_account_first_names, last_name as key_account_last_name, email as key_account_email from cc_users where user_id =:key_account_id"

    set cell_phone ""
    set work_phone ""
    db_0or1row manager_contact "select work_phone as key_account_work_phone, cell_phone as key_account_cell_phone from users_contact where user_id=:key_account_id"


    append account_info_data "$komma{\"logged_user_id\":$logged_user_id, \"logged_user_first_names\":\"$logged_user_first_names\", \"logged_user_last_name\":\"$logged_user_last_name\", \"logged_user_email\":\"$logged_user_email\", \"logged_user_locale\":\"[lang::user::locale -user_id $rest_user_id]\",\"key_account_user_id\":$key_account_id, \"logged_user_company_id\":$company_id, \"logged_user_company_name\":\"$company_name\", \"key_account_first_names\":\"$key_account_first_names\", \"key_account_last_name\":\"$key_account_last_name\",  \"key_account_email\":\"$key_account_email\",  \"key_account_cell_phone\":\"$key_account_cell_phone\", \"key_account_work_phone\":\"$key_account_work_phone\"}"

    set data [list]
    lappend data [cog_rest::json_object]
    return [cog_rest::return_array]
}



ad_proc -public im_rest_get_custom_sencha_invite_new_user {
    -new_user_id
    { -company_id ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }

} {
    Endpoint which is executed in Customer Portal just after app user have added new user.
    Endpoint is not creating new user, just sends invitation email
    
    @param new_user_id newly created user_id to whom we want to send invitation
    @param company_id company which is sending invitation to newly created user

    @return success boolean which outputs if action was successful
    @return invited_user id of invited user
    @return error error message. Has value of "no error" if no error occurs

} {
    set success false
    set error "Username or company is not valid"

    set locale [lang::user::locale -user_id $rest_user_id]

    if {$company_id eq ""} {
        set company_id [im_user_main_company_id -user_id $new_user_id]
    }

    db_1row sender_info "select email as sender_email from cc_users where user_id =:rest_user_id"

    set user_exsits_p [db_0or1row user_info "select first_names, email as receipent_email from cc_users where user_id = :new_user_id"]
    set company_exists_p [db_0or1row user_info "select company_name from im_companies where company_id =:company_id"]

    if {$user_exsits_p && $company_exists_p} {    

        set success true
        set error "no error"
        
        set customer_portal_app_name [parameter::get_from_package_key -package_key "sencha-assignment" -parameter "CustomersPortalAppName" -default "Customers Portal"]
        set default_customer_portal_link "[ad_url]"
        set customer_portal_link [parameter::get_from_package_key -package_key "sencha-assignment" -parameter "CustomersPortalUrl" -default $default_customer_portal_link]
        set password_recovery_link "[ad_url]/register/recover-password"

        set subject "[lang::message::lookup $locale sencha-assignment.lt_customer_portal_invitation_subject]"
        set body "[lang::message::lookup $locale sencha-assignment.lt_customer_portal_invitation_body]"

        intranet_chilkat::send_mail \
            -to_addr $receipent_email \
            -from_addr $sender_email \
            -subject $subject \
            -body $body \
            -no_callback
    }

    set result "{\"success\": $success, \"invited_user\":$new_user_id, \"error\":\"$error\"}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public intranet_rest::get::download_invoice {
    -invoice_id
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
     Endpoint which allows to donwload an invoice.
     Probably needs to be changed later, because auth_token is not correctly working in here, as we are only returning invoice url which still requires user to be logged.
     (endpoint works only when we are logged as user in seperate window)
     
     @param invoice_id integer if of invoice which we want to download_invoice

     @return data object with result of action
     @return_data invoice_url string url of invoice
} {

    set preview_p 0

    db_1row invoice_info "select invoice_nr,last_modified from im_invoices,acs_objects where invoice_id = :invoice_id and invoice_id = object_id"

    set invoice_url "[ad_url][apm_package_url_from_key "intranet-invoices"]pdf?invoice_id=$invoice_id"

    return [cog_rest::json_object]
}


ad_proc -public intranet_rest::get::sencha_i18n {
	{ -package_key ""}
	{ -locale ""}
    -rest_user_id:required
} {
    Endpoint returning i18n translations for given @package_key

    Returns translations json array of translations containing messsage_key, message

    @param package_key string komma separated list of ]project-open[ package keys for which we need translations
    @param locale string locale (e.g. 'de-DE'), if nothing is provided, we use currently logged user locale

    @return messages Array of message key information
    @return_messages message_key string Key of the message used in PO
    @return_messages message string Translated string of the message key
    @return_messages key_of_package string Package key of fetched message
} {

    set translations ""
    set result false

    set queried_package_keys [list]
    foreach key [split $package_key ","] {
        lappend queried_package_keys $key
    }
       
    # Checking for @param:locale param
    if {$locale eq ""} {
        # Getting currently logged user locale
        set locale [lang::user::locale -user_id $rest_user_id]
    } 


    set all_package_keys [db_list get_message_keys "select package_key from apm_packages"]

    # Checking if rest requests contains package_key variable
    if {[llength $queried_package_keys] > 0} {
        # We also need to check if such package_key exists
        set package_exists_p [db_string package_exists "select count(package_id) from apm_packages where package_key in ([template::util::tcl_to_sql_list $queried_package_keys])" -default 0]
        if {$package_exists_p ne 0} {
            set error "no error"
            set result true

            # We must build list of all possible message keys (@param message_key) for given @package_key
            set all_possible_message_keys [list]
            set all_possible_i18n_messages_sql "select message_key, package_key as key_of_package from lang_messages where package_key in ([template::util::tcl_to_sql_list $queried_package_keys]) and locale = 'en_US'"

            db_foreach message_key $all_possible_i18n_messages_sql {
                lappend all_possible_message_keys $message_key
                set message ""
                catch {
                    set message [lang::message::lookup $locale "$key_of_package.$message_key"] 
                    regsub -all {[^[:print:]]} $message {} message 
                    regsub -all {[^[:print:]]} $message_key {} message_key              	  
                }
                lappend messages [cog_rest::json_object]
            }
            return [cog_rest::return_array]
        } else {
            return [cog_rest::error -http_status 500 -message "no package with @package key: '$po_package_key'"]
        }
    }
}



ad_proc -public im_rest_get_custom_register_i18n_message {
    -message_key
    -message
    -package_key
    { -locale ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
     Endpoint which saves translated message to database

     @param message_key message key used to store message translation
     @param message message itself
     @param package_key packey key for which we save messate translation
     @param locale language for which we save message translation

} {

    # Set the variables
    foreach {key value} $query_hash_pairs {
        set $key $value
    }

    if {$locale eq ""} {
        set locale "en_US"
    }

    lang::message::register $locale $package_key $message_key $message

    set result "{\"success\": true}"
    im_rest_doc_return 200 "application/json" $result
    return
}




ad_proc -public intranet_rest::get::package_parameters {
    { -package_id ""}
    { -package_key ""}
    { -rest_user_id 0 }
} {
    Handler for GET rest calls to get package parameters 
    It is possible to query with package_id or package_key
    If neither of these will be provided, endpoint will return all parameters from system


    @param package_id integer id of package of needed package parameters
    @param package_key string key of package of needed package parameters
    
    @return package_parameters Array of package parameters

    @return_package_parameters package_id integer id of package
    @return_package_parameters package_key string key of package
    @return_package_parameters parameter_id integer id of parameter
    @return_package_parameters parameter_name string name of parameter
    @return_package_parameters value string value of parameter
    @return_package_parameters default_value string default value of parameter

} {

    set package_parameters [list]
    set success true

    set package_parameters_sql "select p.package_id, pp.default_value, pp.parameter_id, pp.parameter_name, pp.package_key, pv.attr_value as value from apm_packages p, apm_parameters pp, apm_parameter_values pv where pp.parameter_id = pv.parameter_id and p.package_key = pp.package_key"
    if {$package_id ne ""} {
        append package_parameters_sql " and p.package_id =:package_id"
    } 
    if {$package_key ne ""} {
        append package_parameters_sql " and pp.package_key =:package_key"
    }
    db_foreach package_data $package_parameters_sql {
        lappend package_parameters [cog_rest::json_object]
    }
    
    return [cog_rest::return_array]

}



ad_proc -public intranet_rest::get::company_offices {
    { -company_id ""}
    { -office_type_id ""}
    { -office_status_id ""}
    { -rest_user_id 0 }
} {
    Handler for GET rest calls to get all company offices


    @param company_id integer id of company for which one we want to get offices
    @param office_type_id category "Intranet Office Type" optional argument of office_type in case we want to filter results by office type
    @param office_status_id category "Intranet Office Status" optional argument of office_status in case we want to filter results by office status

    @return offices array of offices

    @return_offices object_id integer office id of office
    @return_offices office_id integer office id of office. Same as object_id
    @return_offices office_type_id integer "Intranet Office Type" type id of office
    @return_offices office_status_id integer "Intranet Office Status" status id  of office
    @return_offices office_name string name of the office
    @return_offices main_office_p string 1 if office is a main office of company, 0 in case its not
} {
    
    if {$company_id eq ""} {
        set company_id [im_user_main_company_id -user_id $rest_user_id]
    }

    set offices [list]

    set additional_sql ""

    if {$office_type_id ne ""} {
       append additional_sql " and office_type_id =:office_type_id"
    }

    if {$office_status_id ne ""} {
       append additional_sql " and office_status_id =:office_status_id"
    }

    set company_offices_sql "select * from im_offices where company_id =:company_id $additional_sql"

    db_foreach company_office $company_offices_sql {

        set object_id $office_id
        
        set main_office_p 0

        # We need procedure which will return office type of 170. Need to add that later
        set main_office_category_id 170
        if {$main_office_category_id eq $office_type_id} {
            set main_office_p 1
        }
        lappend offices [cog_rest::json_object]
    }

    ns_log Warning $offices

    return [cog_rest::return_array]

}


ad_proc -public im_rest_get_custom_sencha_search {
    { -type "all" }
    { -include_deleted_p 0}
    { -query ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls to run search

    @param type type of objects in which we want to search. By default it is "all"
    @param include_deleted_p switch which decides if we should also display deleted objects
    @param query search query
    @param q search query
    @param offset

    @return object_id id of returned object
    @return object_name name of object usually set by "im_category_from_id"
    @return object_type_name object type (e.g. im_project or im_user)
    @return object_type_name_pretty pretty name of object type (e.g. Project or User)


} {
   
    set error ""
    set results_per_page 200
    set offset 0

    set current_user_id $rest_user_id
    set user_id $rest_user_id
    
    set obj_ctr 0
    set main_search_results_list [list]

    set q $query
    
    if {$user_id eq ""} {
        set user_id $rest_user_id
    }

    set package_id [ad_conn package_id]
    set package_url [ad_conn package_url]
    set package_url_with_extras $package_url
    set context [list]
    set context_base_url $package_url

    # Determine the user's group memberships
    set user_is_employee_p [im_user_is_employee_p $current_user_id]
    set user_is_customer_p [im_user_is_customer_p $current_user_id]
    set user_is_wheel_p [im_profile::member_p -profile_id [im_wheel_group_id] -user_id $current_user_id]
    set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $current_user_id]
    set user_is_admin_p [expr $user_is_admin_p || $user_is_wheel_p]


    if {"" == $q} {
        ad_return_complaint 1 [_ search.lt_You_must_specify_some]
    }

    if { $results_per_page <= 0} {
        set results_per_page [ad_parameter -package_id $package_id SearchResultsPerPage -default 20]
    } else {
        set results_per_page $results_per_page
    }

    set limit [expr 100 * $results_per_page]

    if {[lsearch im_document $type] >= 0} {
        set error "Sorry, searching for documents has not been implemented yet."
    }

    # Normalize query - lowercase and without double quotes
    set q [string tolower $q]
    regsub -all {["']} $q {} q


    # Remove accents and other special characters from search query 
    set q [db_exec_plsql normalize "select norm_text(:q)"]
    set query $q
    #set q [join $q " & "]

    # Determine if there are several parts to the query
    set nquery [llength $q]

    # Set default values
    set high 0
    set count 1
    set num_results 0
    set elapsed 0

    set urlencoded_query [ad_urlencode $q]
    if { $offset < 0 } { set offset 0 }
    set t0 [clock clicks -milliseconds]


        set limit [expr 100 * $results_per_page]

        # Prepare the list of searchable object types
        set sql "
            select
                sot.object_type_id,
                aot.object_type,
                aot.pretty_name as object_type_pretty_name,
                aot.pretty_plural as object_type_pretty_plural
            from
                im_search_object_types sot,
                acs_object_types aot
            where
                sot.object_type = aot.object_type
        "

        # Permissions for different types of business objects

        # Projects
        set project_perm_sql "
                    and p.project_id in (
                            select
                                    p.project_id
                            from
                                    im_projects p,
                                    acs_rels r
                            where
                                    r.object_id_one = p.project_id
                                    and r.object_id_two = :current_user_id
                    )"

        if {[im_permission $current_user_id "view_projects_all"]} {
                set project_perm_sql ""
        }


        # Companies 
        set company_perm_sql "
                    and c.company_id in (
                            select  c.company_id
                            from    im_companies c,
                                    acs_rels r
                            where   r.object_id_one = c.company_id
                                    and r.object_id_two = :current_user_id
                            and c.company_status_id not in ([im_company_status_deleted])
                    )"

        if {[im_permission $current_user_id "view_companies_all"]} {
                set company_perm_sql "
                    and c.company_status_id not in ([im_company_status_deleted])
            "
        }



        # Conf Items
        set conf_item_perm_sql "
                    and c.conf_item_id in (
                            select  c.conf_item_id
                            from    im_conf_items c,
                                    acs_rels r
                            where   r.object_id_one = c.conf_item_id
                                    and r.object_id_two = :current_user_id
                            and c.conf_item_status_id not in (11702)
                    )"

        if {[im_permission $current_user_id "view_conf_items_all"]} {
                set conf_item_perm_sql "
                    and c.conf_item_status_id not in ([im_conf_item_status_deleted])
            "
        }


        # Events 
        set event_perm_sql "
                    and e.event_id in (
                            select  c.event_id
                            from    im_events c,
                                    acs_rels r
                            where   r.object_id_one = c.event_id
                                    and r.object_id_two = :current_user_id
                            and c.event_status_id not in (82099) -- 82099=im_event_status_deleted
                    )"

        if {[im_permission $current_user_id "view_events_all"]} {
                set event_perm_sql "
                    and e.event_status_id not in ([im_event_status_deleted])
            "
        }


    # Financial Documents 
    set customer_sql "
        select distinct
            c.company_id
        from
            im_companies c,
            acs_rels r
        where
            c.company_type_id in ([join [im_sub_categories [im_company_type_customer]] ","])
            and r.object_id_one = c.company_id
            and r.object_id_two = :current_user_id
            and c.company_path != 'internal'
    "
    if {![im_user_is_customer_p $current_user_id]} { set customer_sql "select 0 as company_id" }


    set provider_sql "
        select distinct
            c.company_id
        from
            im_companies c,
            acs_rels r
        where
            c.company_type_id in ([join [im_sub_categories [im_company_type_provider]] ","])
            and r.object_id_one = c.company_id
            and r.object_id_two = :current_user_id
            and c.company_path != 'internal'
    "
    if {![im_user_is_freelance_p $current_user_id]} { set provider_sql "select 0 as company_id" }


    set invoice_perm_sql "
                and i.invoice_id in (
                    select  i.invoice_id
                    from    im_invoices i,
                        im_costs c
                    where   i.invoice_id = c.cost_id
                        and (
                            c.customer_id in ($customer_sql)
                        OR
                            c.provider_id in ($provider_sql)
                        )
                )"

    if {[im_permission $current_user_id "view_invoices"]} {
        set invoice_perm_sql ""
    }


    # Users
    set user_perm_sql "
                and person_id not in (
    select distinct
        cc.user_id
    from
        cc_users cc,
        (
            select  group_id
            from    groups
            where   group_id > 0
                and 'f' = im_object_permission_p(group_id,8849,'read')
        ) forbidden_groups,
        group_approved_member_map gamm
    where
        cc.user_id = gamm.member_id
        and gamm.group_id = forbidden_groups.group_id
                )"

    if {[im_permission $current_user_id "view_users_all"]} {
        set user_perm_sql ""
    }

    # user_perm_sql is very slow (~20 seconds), so
    # just leave the permission check for later...
    set user_perm_sql ""

    # Remember to hide deleted users
    set deleted_users_sql "
        and p.person_id not in (
            select  m.member_id
            from    group_member_map m, 
                membership_rels mr
            where   m.group_id = acs__magic_object_id('registered_users') 
                AND m.rel_id = mr.rel_id 
                AND m.container_id = m.group_id 
                AND m.rel_type::text = 'membership_rel'
                AND mr.member_state != 'approved'
        )
    "
    if {1 == $include_deleted_p} {
        set deleted_users_sql ""
    }


    #  Files
    set file_perm_sql "
                and p.file_id in (
                        select
                                p.file_id
                        from
                                im_files p,
                                acs_rels r
                        where
                                r.object_id_one = p.file_id
                                and r.object_id_two = :current_user_id
                )"

    if {[im_permission $current_user_id "view_projects_all"]} {
            set file_perm_sql ""
    }

    # Forums 
    set forum_perm_sql ""

    # Build select for object types

    foreach t $type {

        # Security check for cross site scripting
        if {![regexp {^[a-zA-Z0-9_]*$} $t]} {
        im_security_alert \
            -location "/intranet-search-pg/www/search.tcl" \
            -message "Invalid search object type - SQL injection attempt" \
            -value [ns_quotehtml $t]
        # Quote the harmful var
        regsub -all {[^a-zA-Z0-9_]} $t {_} t
        }
        
        lappend types "'$t'"
    } 

    set object_type_where "object_type in ([join $types ","])"
    if {[string equal "all" $type]} {
        set object_type_where "1=1"
    }

    # Main SQL

    set conf_item_union ""
    if {[im_table_exists im_conf_items]} {
        set conf_item_union "
                UNION
                select  conf_item_id as object_id,
                    'im_conf_item' as object_type,
                                    0 as object_sub_type_id
                from    im_conf_items c
                where   1=1
                    $conf_item_perm_sql
        "
    }

    set event_union ""
    if {[im_table_exists im_events]} {
        set event_union "
                UNION
                select  event_id as object_id,
                    'im_event' as object_type,
                                    0 as object_sub_type_id
                from    im_events e
                where   1=1
                    $event_perm_sql
        "
    }

    set q [join [split $q " "] " & "]

    set sql "
        select
            acs_object__name(so.object_id) as name,
            acs_object__name(so.biz_object_id) as biz_object_name,
            (rank(so.fti, :q::tsquery) * sot.rel_weight)::numeric(12,2) as rank,
            fti as full_text_index,
            bou.url,
            so.object_id,
            sot.object_type,
            aot.pretty_name as object_type_pretty_name,
            so.biz_object_id,
            so.popularity,
            readable_biz_objs.object_type as biz_object_type,
                    readable_biz_objs.object_sub_type_id as object_sub_type_id
        from
            im_search_objects so,
            acs_object_types aot,
            (   select  *
                from    im_search_object_types 
                where   $object_type_where
            ) sot,
            (
                select  project_id as object_id,
                    'im_project' as object_type,
                    0 as object_sub_type_id
                from    im_projects p
                where   1=1
                    $project_perm_sql
                UNION
                select  company_id as object_id,
                    'im_company' as object_type,
                                    0 as object_sub_type_id
                from    im_companies c
                where   1=1
                    $company_perm_sql
                UNION
                select  invoice_id as object_id,
                    'im_invoice' as object_type,
                                    c.cost_type_id as object_sub_type_id
                from    im_invoices i,
                    im_costs c
                where   i.invoice_id = c.cost_id
                    $invoice_perm_sql
                UNION
                select  person_id as object_id,
                    'user' as object_type,
                                    0 as object_sub_type_id
                from    persons p
                where   1=1
                    $deleted_users_sql
                    $user_perm_sql
                        UNION
                            select  item_id as object_id,
                                    'content_item' as object_type,
                                    0 as object_sub_type_id
                            from    cr_items c
                            where   1=1
                $conf_item_union
                $event_union
            ) readable_biz_objs,
            acs_objects o
            LEFT OUTER JOIN (
                select  *
                from    im_biz_object_urls
                where   url_type = 'view'
            ) bou ON (o.object_type = bou.object_type)
        where
            readable_biz_objs.object_id = o.object_id 
            and so.object_type_id = sot.object_type_id
            and sot.object_type = aot.object_type
            and so.biz_object_id = readable_biz_objs.object_id
            and so.fti @@ to_tsquery('default',:q)
        order by
            (rank(so.fti, :q::tsquery) * sot.rel_weight) DESC
        offset :offset
        limit :limit
    "

    db_foreach full_text_query $sql {
        
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        # Localize the object type
        regsub -all { } $object_type_pretty_name {_} object_type_pretty_name_sub
        set object_type_pretty_name [lang::message::lookup "" intranet-core.$object_type_pretty_name_sub $object_type_pretty_name]


        if {$count > $results_per_page} {
            continue
        }

        if {"" != $url} {
        set name_link $name
        }
        
        set text [im_tsvector_to_headline $full_text_index]
        set headline [db_string headline "select headline(:text, :q::tsquery)" -default ""]

        # Final permission test: Make sure no object slips through security
        # even if it's kind of slow to do this iteratively...
        switch $object_type {
        im_project { 
            im_project_permissions $current_user_id $object_id view read write admin
            set object_type_pretty_name [lang::message::lookup "" intranet-timesheet2-workflow.Project $object_type_pretty_name]
            if {!$read} { continue }
        }
        user { 
            im_user_permissions $current_user_id $object_id view read write admin
            if {!$read} { continue }
        }
        im_company {
            im_project_permissions $current_user_id $object_id view read write admin
            set object_type_pretty_name [lang::message::lookup "" intranet-translation.Company $object_type_pretty_name]
            if {!$read} { continue }            
        }
        im_fs_file { 
            # The file is readable if it's business object is readable
            # AND if the folder is readable

            # Very ugly: The biz_object_id is not checked for "user"
            # because it is very slow... So check it here now.
            if {"user" == $biz_object_type} {
            im_user_permissions $current_user_id $biz_object_id view read write admin
            if {!$read} { continue }
            }

            # Determine the permissions for the file
            set file_permission_p 0
            db_0or1row forum_perm "
            select  f.filename,
                '1' as file_permission_p
            from    im_fs_files f
            where   f.file_id = :object_id
            "
            if {!$file_permission_p} { continue }

            # Only with files - biz_object_id==0 means Home Filestorage
    #       if {0 == $biz_object_id} { set biz_object_name [lang::message::lookup "" intranet-fs.Home_Filestorage "Home Filestorage"] }

            set name_link "$filename"
        }
        im_forum_topic { 
            # The topic is readable if it's business object is readable
            # AND if the user belongs to the right "sphere"

            # Very ugly: The biz_object_id is not checked for "user"
            # because it is very slow... So check it here now.
            if {"user" == $biz_object_type} {
            im_user_permissions $current_user_id $biz_object_id view read write admin
            if {!$read} { continue }
            }

            # Determine if the current user belongs to the admins of
            # the "business object". This is necessary, because there
            # is the forum permission "PM Only" which gives rights only"
            # to the (project) managers of the of the container biz object
            set object_admin_sql "
                    ( select count(*) 
                      from  acs_rels r,
                        im_biz_object_members m
                      where r.object_id_two = :current_user_id
                        and r.object_id_one = :biz_object_id
                        and r.rel_id = m.rel_id
                        and m.object_role_id in (1301, 1302, 1303)
                    )::integer\n"
            if {$user_is_admin_p} { set object_admin_sql "1::integer\n" }

            # 070802 fraber: This line fixes a  where
            # the im_search_objects contains a forum line that doesn't exist.
            # However, I couldn't reproduce how the line got there.
            set forum_permission_p 0

            # Determine the permissions for the forum item
            db_0or1row forum_perm "
            select
                t.subject,
                im_forum_permission(
                    :current_user_id::integer,
                    t.owner_id,
                    t.asignee_id,
                    t.object_id,
                    t.scope,
                    1::integer,
                    $object_admin_sql ,
                    :user_is_employee_p::integer,
                    :user_is_customer_p::integer
                ) as forum_permission_p
            from
                im_forum_topics t
            where
                t.topic_id = :object_id
            "
            if {!$forum_permission_p} { continue }
            set name_link "$subject"

        }
        content_item {
            db_1row content_item_detail "
                   select   name, content_type
                   from cr_items 
                   where    item_id = :object_id
                "

            regsub -all { } $content_type {_} content_type_sub
            regsub -all {:} $content_type_sub {} content_type_sub
            set object_type_pretty_name [lang::message::lookup "" intranet-translation.ContentItem_$content_type_sub $content_type]

            switch $content_type {
            "content_revision" {
                # Wiki
                set read_p [permission::permission_p \
                        -object_id $object_id \
                        -party_id $current_user_id \
                        -privilege "read" ]

                if {!$read_p} { continue }
                set name_link "$name"
            } 
            "workflow_case_log_entry" {
                # Bug-Tracker
                set bug_number [db_string bug_from_cr_item "
                            select bug_number from bt_bugs,cr_items where item_id=:object_id and cr_items.parent_id=bug_id
                        "]
                if {!$bug_number} { continue }
                #set name_link "<a href=\"/bug-tracker/bug?bug_number=$bug_number\">bug: $bug_number $name</a>"
                set name_link $bug_number $name
            }
            "::xowiki::Page" {
                set page_name ""
                set package_mount ""
                db_0or1row page_info "
                select  s.name as package_mount,
                    i.name as page_name
                from    cr_items i,
                    apm_packages p,
                    site_nodes s, xowiki_pagex d
                where   i.item_id = :object_id and
                    p.package_id = s.object_id and
                    p.package_key = 'xowiki' and 
                    p.package_id = d.object_package_id and 
                    i.item_id = d.item_id and
                    i.live_revision = d.revision_id
                "
                #set name_link "<a href=\"/$package_mount/$page_name\">$page_name</a>"
                set name_link $page_name
            }
            "::xowiki::FormPage" {
                # Skip FormPage contents
                continue
            }
            default {
                set name_link [lang::message::lookup "" intranet-search-pg.Unknown_CI_Type "unknown content_item type: %content_type%"]
            }
            }
        }
        }

        set object_name $name_link
        set object_type_name_pretty $object_type_pretty_name
        set object_type_name $object_type
      
        lappend main_search_results_list [im_rest_json_object -proc_name im_rest_get_custom_sencha_search]

        incr obj_ctr

    }

    set main_search_results [join $main_search_results_list ","]

    if {$error ne ""} {
        set success false
    } else {
        set success true
    }

    set result "{\"success\": $success, \"total\":$obj_ctr, \n\"data\": \[\n$main_search_results\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}



ad_proc -public im_rest_get_custom_sencha_validate_field {
    -field
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Rest endpoint which allows to validate field in realtime
} {
    set result ""
    
    array set hash_array $query_hash_pairs

    set optional_vars [list project_id email]
    foreach var $optional_vars {
        if {![info exists hash_array($var)]} {
            set $var ""
        } else {
            set $var $hash_array($var)
        }
    }

    switch $field {
        email {
            if {$email ne "" && $project_id ne ""} {
                set result [event_management_participant_email_already_registered -project_id $project_id -email $email]
                set success 1
                if {$result ne 0} {
                    set project_name [im_name_from_id $project_id]
                    set default_message "Such email is already registered to $project_name"
                    set message [lang::message::lookup "" intranet-event-management.email_already_registered_in_event $default_message]
                } else {
                    set message "Ok"
                }               
            }
        }
        default {
            set success 0
            set message "Wrong field name"
        }
}
   
    set result "{\"success\": $success, \"message\": \"$message\", \"result\": \"$result\"}"
    im_rest_doc_return 200 "application/json" $result
    return
}




ad_proc -public intranet_rest::get::sencha_dynview_columns {
    { -view_name ""}
    -rest_user_id:required
} {
    Return a list of columns for a view

    @param view_name string Name of the view to look for
    @param view_id integer ID of the view in case a view_name is not provided

    @return views Array of views
    @return_views column_name string name of the column
    @return_views column_name_translated string Translated name of the column
    @return_views variable_name string Name of the variable
    @return_views column_render string Rendering tcl of the column
    @return_views datatype string Datatype of the column
    @return_views sort_order integer Place of the column
    @return_views ajax_configuration string Configuration in AJAX

} {

    set user_id $rest_user_id
    set locale [lang::user::locale -user_id $rest_user_id]

    if {$view_name ne ""} {
        set view_id [im_view_id_from_name $view_name]
    } else {
        if {$view_id eq ""} {
            cog_rest::error -http_status 400 -message "You must provide view_id or view_name"
        }
    }
    
    # Check if such view exist
    db_1row view_checker "select count(*) as count_views from im_views where view_id = :view_id"

    set json_columns ""
    if {$count_views ne 0} {
        set sql_where "v.view_id=:view_id"
        set column_sql "select v.view_id, v.view_name, vc.sort_order, vc.column_name, vc.column_render_tcl, vc.variable_name, vc.datatype, vc.visible_for, vc.ajax_configuration, v.view_label
            from im_view_columns vc
            inner join im_views v on (v.view_id = vc.view_id)
            where $sql_where
            and group_id is null
            order by sort_order
            "
        set obj_ctr 0
        db_foreach column_list_sql $column_sql {
            set temp {}
            set cmd "append temp $visible_for"
            set is_visible_p [eval $cmd]
            if {$is_visible_p ne 0} {
                set komma ",\n"
                if {0 == $obj_ctr} { set komma "" }
                set ajax_configuration_json ""
                append ajax_configuration_json ""
                set ajax_configuration_values [split $ajax_configuration ","]
                set ajax_config_obj_ctrl 0
                foreach ajax_val $ajax_configuration_values {
                    set key_and_value [split $ajax_val ":"]
                    set key [lindex $key_and_value 0]
                    set value [lindex $key_and_value 1]
                    if {[string first {[} $value] == 0} {
                        eval "set value $value"
                    }
                    set variable_key "intranet-core.[lang::util::suggest_key $value]"
                   # set value [lang::message::lookup $locale $variable_key $value]


                    set komma_for_ajax_config ",\n"
                    if {0 == $ajax_config_obj_ctrl} { set komma_for_ajax_config "" }
                    append ajax_configuration_json "$komma_for_ajax_config{\"config_key\": \"[im_quotejson $key]\",\"config_value\": \"$value\"}"
                    incr ajax_config_obj_ctrl
                }
                regsub -all {#} $column_name {} column_name_trimmed
                # Check if column translation is not combined of multiple ones. We do this by counting hash chars ('#')
                set number_of_hashes [expr {[llength [split $column_name "#"]] - 1}]
                if {$number_of_hashes > 2 } {
                    set combined_translation ""
                    foreach single_trans [split $column_name_trimmed " "] {
                        lappend combined_translation "[lang::message::lookup $locale $single_trans]"                    
                    }
                    set column_name_translated $combined_translation

                } else {
                     set column_name_translated "[lang::message::lookup $locale $column_name_trimmed]"
                }

                lappend views [cog_rest::json_object]
            }
        }
        set total_columns $obj_ctr
    } else {
        cog_rest::error -http_status 400 -message "View not found"
    }

    return [cog_rest::return_array]
}


ad_proc -public im_rest_get_custom_dynview_rows {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get dynview columns
} {

    set user_id $rest_user_id
    set locale [lang::user::locale -user_id $user_id]

    array set query_hash $query_hash_pairs

    set possible_vars [list assignee_id project_id company_id]
    foreach v $possible_vars {
        if {[info exists query_hash($v)]} {
	    set $v "$query_hash($v)"
        }
    }

    set translated_cats [list project_type_name subject_area_name project_status_name]

    set view_id 0
    set total_rows 0
    set error_msg ""

    set json_rows ""

    # Default filters
    set project_status_id [im_project_status_open]
    set company_id ""
    set start_date ""
    set end_date ""
    set include_subprojects_p 0
    set mine_p "f"
    set cur_format [im_l10n_sql_currency_format]

    # Order clause
    set order_by_clause ""

    if {"" == $start_date} { 
       # set start_date [parameter::get_from_package_key -package_key "intranet-cost" -parameter DefaultStartDate -default "2000-01-01"] 
    }
    if {"" == $end_date} { 
        #set end_date [parameter::get_from_package_key -package_key "intranet-cost" -parameter DefaultEndDate -default "2100-01-01"]
        
    } else {
        set order_by_clause "" 
    }

    # Building custom variables form ULR (e.g. company_id)
    for {set x 0} {$x<[llength $query_hash_pairs]} {set x [expr {$x+2}]} {
        if {[lindex $query_hash_pairs $x] ne "view_id" && [lindex $query_hash_pairs $x] ne "view_name"} {
            set [lindex $query_hash_pairs $x] [lindex $query_hash_pairs [expr $x+1]]
        }
    }

    if {[exists_and_not_null query_hash(view_name)]} {
        set view_id [im_view_id_from_name $query_hash(view_name)]
    } else {
        set error_msg "You must provide view_id or view_name"
    }

    # If view_id is provided
    if {[exists_and_not_null query_hash(view_id)]} {
        set view_id $query_hash(view_id)
    } else {
        set error_msg "You must provide view_id or view_name"
    }

    # Check if such view exist
    db_1row view_checker "select count(*) as count_views from im_views where view_id = :view_id"

          
    # Getting View sql, returning error when view_sql not exist
    set main_view_sql_query [db_string view_sql "select view_sql from im_views where view_id = :view_id" -default ""]

    if {$main_view_sql_query eq ""} {
        set main_view_sql_query "select p.* from im_projects p"
    }


    set main_extra_select ""
    set main_extra_from ""
    set main_extra_where ""


    if {true} {
        set variables_sql "select extra_select, extra_from, extra_where, variable_name, column_render_tcl, visible_for, (select count(*) from im_categories where variable_name = category) as is_category from im_view_columns where view_id = :view_id"
    
        set obj_ctr 0

        db_foreach view_variables $variables_sql {

            set komma ",\n"
            if {0 == $obj_ctr} { set komma ",\n" }
            if { ![empty_string_p $extra_select] } {
                
                append main_extra_select "$komma $extra_select"
            }           
            if { ![empty_string_p $extra_from] } {
                set main_extra_from"$komma $extra_from"
            } 
            if { ![empty_string_p $extra_where] } {
                set main_extra_where "$komma $extra_where"
            } 
            incr obj_ctr
            
        }


        set criteria [list]
        if { ![empty_string_p $project_status_id] && $project_status_id > 0 } {
            #lappend criteria "p.project_status_id in ([join [im_sub_categories $project_status_id] ","])"
        }
       # if { ![empty_string_p $project_type_id] && $project_type_id != 0 } {
        #    lappend criteria "p.project_type_id in ([join [im_sub_categories $project_type_id] ","])"
        #}

        if { ![empty_string_p $company_id] && $company_id != 0 } {
            lappend criteria "p.company_id=:company_id"
        }

        # Limit to start-date and end-date
        #if {"" != $start_date} { lappend criteria "p.end_date::date >= :start_date" }
        #if {"" != $end_date} { lappend criteria "p.start_date::date <= :end_date" }


        if { $include_subprojects_p == "f" } {
            lappend criteria "p.parent_id is null"
        }

        set where_clause [join $criteria " and\n            "]
        if { ![empty_string_p $where_clause] } {
            set where_clause " and $where_clause"
        }
        set sql "
        SELECT *
        FROM
                ( SELECT
                        p.*,
                        c.company_id as company_id,
                        c.company_name
                $main_extra_select
                FROM
                    ($main_view_sql_query) p,
                    im_companies c
                    $main_extra_from
                WHERE
                        p.company_id = c.company_id
                and p.project_type_id not in ([im_project_type_task], [im_project_type_ticket])
                $where_clause
                    $main_extra_where
                ) projects
                $order_by_clause
        "

        set obj_ctr 0
        db_foreach row_sql $sql {
 
            set total_visible_vars 0
            set komma ",\n"
            if {0 == $obj_ctr} { set komma "" }
            set inner_obj_ctrl 1
            set count [llength variables_sql]
            append json_rows "$komma{"
            # First loop to ONLY count visible vars from im_view_columns
            db_foreach variables_sql $variables_sql {
                set is_visible_p "[eval $visible_for]"
                if {$is_visible_p ne 0} {
                    incr total_visible_vars
                }
            }
            if {[exists_and_not_null project_id]} {
                append json_rows "\"project_id\": \"[im_quotejson $project_id]\","
            }
            if {[exists_and_not_null project_lead_id]} {
                append json_rows "\"project_lead_id\": \"[im_quotejson $project_lead_id]\","
            }
            if {[exists_and_not_null company_id]} {
                append json_rows "\"company_id\": \"[im_quotejson $company_id]\","
            }
            if {[exists_and_not_null project_status_name]} {
                append json_rows "\"project_status_id\": \"[im_quotejson $project_status_id]\","
            }
            
            if {[exists_and_not_null project_status_id]} {
                append json_rows "\"project_status_name\": \"[im_quotejson [im_category_from_id -current_user_id $rest_user_id $project_status_id]]\","
            }

            # Second loop to collect data (rows)
            db_foreach variables_sql $variables_sql {
                set is_visible_p "[eval $visible_for]"
                if {$is_visible_p ne 0} {
                    eval "set v $$variable_name"
                    #eval "set v $column_render_tcl"

                    set variable_key "intranet-core.[lang::util::suggest_key $v]"

                    set v_translated [lang::message::lookup $locale $variable_key $v]

                    if {[lsearch $translated_cats $variable_name]>-1} {
                        set v $v_translated
                    }

                    if {$inner_obj_ctrl == $total_visible_vars} {
                        append json_rows "\"[im_quotejson $variable_name]\": \"[im_quotejson $v]\""
                    } else {
                        append json_rows "\"[im_quotejson $variable_name]\": \"[im_quotejson $v]\","
                    }
                    incr inner_obj_ctrl
                } 
            }
            append json_rows "}"
            incr obj_ctr
        }
        set total_rows $obj_ctr
        return  "{\"success\": true, \"view_id\": \"$view_id\", \"total_rows\": \"$total_rows\", \n\"data\": \[\n$json_rows\n\]\n}"

    } else {
        set error_msg "view_sql not found"
        return [cog_rest::error -http_status 500 -message $error_msg]
    }

}


ad_proc -public im_rest_get_custom_sencha_dynfields {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get dynfiels for given @param page_url
} {

    set user_id $rest_user_id
    array set query_hash $query_hash_pairs

    if {[exists_and_not_null query_hash(obj_type)]} {
       set obj_type $query_hash(obj_type)
    } else {
        set obj_type "none"
    }

    if {[exists_and_not_null query_hash(page_url]} {
       set page_url $query_hash(page_url)
    } else {
        set page_url "default"
    }

    set attributes [sencha_dynfields::elements -object_type $obj_type -page_url $page_url]
    set obj_ctr 0
    set attributes_json ""
    
    foreach var $attributes {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }

        append attributes_json "$komma{\"attribute_name\": \"[lindex $var 0]\", \"attribute_label\": \"[lindex $var 1]\", \"datatype\": \"[lindex $var 2]\", \"x_position\": \"[lindex $var 3]\", \"y_position\": \"[lindex $var 4]\", \"widget_name\": \"[lindex $var 5]\", \"label_style\": \"[lindex $var 6]\", \"required\": \"[lindex $var 7]\", \"default_value\": \"[lindex $var 8]\"}"
        incr obj_ctr
    }

    set result "{\"success\": true, \"total\": \"$obj_ctr\", \n\"data\": \[\n$attributes_json\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
   
   }




ad_proc -public im_rest_get_custom_sencha_view_types {
    { -view_type_id 0}
    { -view_type_name "all"}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest call to get dynview columns

    @param view_type_id id of view types we want to get
    @param view_type_name name of view types we want to get

    @return view_id view id
    @return view_name view name
    @return view_label human readable view label

} {
    set views_results ""
    set success true

    set sql_where ""

    if {$view_type_id == 0} {
        if {$view_type_name ne "all"} {
            set sql_where "where c.category_id = v.view_type_id and c.aux_string1 =:view_type_name"
        }
    } else {
        set sql_where "where c.category_id = v.view_type_id and view_type_id =:view_type_id"
    }

    set base_views_sql "select distinct view_id, view_name, view_label, sencha_configuration from im_views v, im_categories c $sql_where "
    set obj_ctr 0


    db_foreach view $base_views_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append views_results "$komma{\"view_id\":$view_id, \"view_name\":\"$view_name\", \"view_label\":\"$view_label\", \"sencha_configuration\": \"$sencha_configuration\"}"
        incr obj_ctr     
    }


    set result "{\"success\": $success,  \n\"data\": \[\n$views_results\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_send_reset_link {
    -email
    { -app_name ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Endpoint which takes user email and send him email with password reset link

} {
    set success false

    
    set password_token [db_string user_id_from_email "select password from cc_users where email =:email" -default ""]

    if {$password_token ne ""} {

        set default_customer_portal_link "[ad_url]"

        switch $app_name {
            "CustomersPortal" {
                set app_name [parameter::get_from_package_key -package_key "sencha-poral" -parameter "CustomersPortalAppName" -default "Customers Portal"]
                set portal_url [parameter::get_from_package_key -package_key "sencha-portal" -parameter "CustomersPortalUrl" -default $default_customer_portal_link]
                set company_id [im_user_main_company_id -user_id [db_string user_id "select party_id from parties where email = :email"]]
                set from_addr [db_string get_key_account_mail "select email from im_companies, parties where party_id = manager_id and company_id =:company_id" -default ""]      

            }
            "ProjectManagersPortal" {
                set app_name [parameter::get_from_package_key -package_key "sencha-poral" -parameter "ProjectManagersPortalAppName" -default "Project Managers Portal"]
                set portal_url [parameter::get_from_package_key -package_key "sencha-portal" -parameter "ProjectManagersPortalUrl" -default $default_customer_portal_link]
                set from_addr [db_string employee_from "select email from im_employees, parties where supervisor_id = party_id and employee_id =:rest_user_id" -default ""]
            }
            "FreelancersPortal" {
                set app_name [parameter::get_from_package_key -package_key "sencha-poral" -parameter "FreelancersPortalAppName" -default "Freelancers Portal"]
                set portal_url [parameter::get_from_package_key -package_key "sencha-portal" -parameter "FreelancersPortalUrl" -default $default_customer_portal_link]
                set from_addr [parameter::get_from_package_key -package_key "sencha-portal" -parameter "GeneralFreelancerContact" -default ""]
            }
        }

        if {$from_addr eq ""} {set from_addr [ad_admin_owner]}

        set password_reset_url "$portal_url?email=$email&fp_token=$password_token"
        
        # Get user info for email
        db_1row user_info "select user_id, first_names, last_name from cc_users where email =:email"
        

        set user_id [db_string user_id_from_email "select user_id from cc_users where email =:email" -default ""]
        if {$user_id ne ""} {
            set locale [lang::user::locale -user_id $user_id]
            set subject "[lang::message::lookup $locale sencha-portal.forgot_password_email_subject]"
            set body "[lang::message::lookup $locale sencha-portal.forgot_password_email_body]"
       }
        
        catch {
            intranet_chilkat::send_mail \
                -to_addr $email \
                -from_addr $from_addr \
                -subject $subject \
                -body $body \
                -no_callback
        }

        set success true

    }

    if {$success eq "true"} {
	set result "{\"success\": $success, \"password_reset_url\":\"$password_reset_url\"}"
    } else {
	set result "{\"success\": $success}"
    }
    im_rest_doc_return 200 "application/json" $result
    return

}


ad_proc -public im_rest_get_custom_sencha_verify_forgot_password_token {
    -email
    -token
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Endpoint which verifies if user token and email are valid.

    @param email user email
    @param token user token
} {
    set success false
    set message "Wrong token"

    set token_matched_p [im_verify_user_token -email $email -token $token]

    if {$token_matched_p} {
        set success true
        set message "ok"
    }

    set result "{\"success\": $success, \"message\":\"$message\"}"
    im_rest_doc_return 200 "application/json" $result
    return

}



ad_proc -public im_rest_get_custom_sencha_update_password {
    -email
    -token
    -new_password
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Endpoint which finaly changes user password.
    Token and email are used for veryfication

    @param email user email
    @param token user token
    @param new_password new user password
} {
    set success false
    
    set user_id [db_string user_id_from_email "select user_id from cc_users where email =:email and password =:token" -default ""]
    set username [db_string user_id_from_email "select username from cc_users where email =:email and password =:token" -default ""]

    if {$user_id ne "" && $username ne ""} {
        acs_user::get -user_id $user_id -array user
        set authority_id $user(authority_id)
        auth::password::ChangePassword -authority_id $authority_id -username $username -new_password $new_password
        set success true
    }

    set result "{\"success\": $success}"
    im_rest_doc_return 200 "application/json" $result
    return

}




ad_proc -public im_rest_get_custom_freelancer_financial_documents {
    { -assignee_id ""}
    { -cost_type_id ""}
    { -cost_status_id ""}
    { -data_only_p 0}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -query_hash_pairs {} }
    { -debug 0 }
} {
    Handler for GET rest calls which returns freelancer financial documents
    This procedure is currently (04.03.2020) NOT used, instead we have additional (optional) sql query in 'im_rest_get_custom_project_finance_documents'
    For now leaving it in code as it might come in handy

} {

    set success true
    set bills_result ""

    # First we get project_ids 
    set project_ids [list]

    if {$assignee_id eq ""} {
        set assignee_id $rest_user_id
    }
    
    set obj_ctr 0
    set bills_sql "select cost_id, cost_nr, cost_name, amount, cost_status_id, cost_type_id, im_name_from_id(cost_status_id) as cost_status_name, currency, im_name_from_id(cost_type_id) as cost_type_name, c.project_id, im_name_from_id(c.project_id) as project_name from im_costs c, im_freelance_assignments fa, im_freelance_packages fp where fa.freelance_package_id = fp.freelance_package_id and fa.purchase_order_id = c.cost_id and fa.assignee_id =:assignee_id"
    db_foreach bill $bills_sql {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append bills_result "$komma{\"cost_id\":\"$cost_id\",\"cost_nr\":\"$cost_nr\",\"cost_name\":\"$cost_name\", \"cost_type_id\":\"$cost_type_id\",\"cost_status_id\":\"$cost_status_id\",\"cost_status_name\":\"$cost_status_name\",\"cost_type_name\":\"$cost_type_name\",\"project_id\":\"$project_id\",\"project_name\":\"$project_name\", \"amount\":\"$amount\", \"currency\":\"$currency\"}"
        incr obj_ctr    
    }

    set result "{\"success\": $success, \n\"data\": \[\n$bills_result\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return  


}


ad_proc -public intranet_rest::get::customers_companies {
    -email
    -rest_user_id:required
} {
    Handler for GET rest calls to get company_id by querying with only email

    @param email string to be used with autocomplete query

    @return customer_companies array with customer companies
    @return_customer_companies company_id integer id of company
    @return_customer_companies email string email of company

} {

    set customer_companies [list]

    if {$email ne ""} {

        set customers_sql "select user_id, email from cc_users where email LIKE lower('$email%')"

        db_foreach customer $customers_sql {
            if {[im_user_is_customer_p $user_id]} {
                set company_id [im_user_main_company_id -user_id $user_id]
                if {$company_id ne ""} {
                    lappend customer_companies [cog_rest::json_object]
                }
            }
        }

    }
    return [cog_rest::return_array]
}


